% Vorlesung am 09.07.2014
% TeX: Michi

\renewcommand{\printfile}{2014-07-02}

\minisec{Alternative Herleitung des Bogoliubov-Spektrum}

Die Kontinuitätsgleichung ist durch 
%
\begin{align}
  \frac{\partial n}{\partial t} + \div(n\cdot\bm{v}) = 0 \label{eq:2014-07-09-1} 
\end{align}
%
gegeben. Die Eulergleichung ist
%
\begin{align}
  m \frac{\partial \bm{v}}{\partial t} + \nabla \left( \frac{m v^2}{2} + gn + V_{ext} - \frac{\hbar^2}{2m} \frac{\Delta \sqrt{n}}{\sqrt{n}} \right) &= 0.\label{eq:2014-07-09-2}
\end{align}
%
Jetzt linearisieren wir um eine homogene Dichte $n_0$
%
\begin{align}
  n(\bm{r},t) &= n_0 + \delta n(\bm{r},t) \quad\text{und}\quad \delta n \ll n_0.
\end{align}
%
Damit in Gleichung~\eqref{eq:2014-07-09-1} und~\eqref{eq:2014-07-09-2} und behalte nur lineare Terme in $\delta n$, $\bm{v}$ führt zu
%
\begin{align}
  \partial_t \delta n + n_0 \nabla \cdot \bm{v} &= 0 \\
  m \partial_t \bm{v} + \nabla\left(g \delta n - \frac{\hbar^2}{4m n_0} \Delta \delta n\right) = 0. \label{eq:2014-07-09-3}
\end{align}
%
Lösungsansatz mittels ebener Wellen 
%
\begin{align*}
  \delta n &= \delta n_1 \ee^{\ii(\bm{k} \cdot \bm{r} - \omega t)}, \\
  \bm{v} &= \bm{v}_1 \ee^{\ii(\bm{k} \cdot \bm{r} - \omega t)}.
\end{align*}
%
Eingesetzt in Gleichung~\eqref{eq:2014-07-09-3}
%
\begin{align}
  - \ii \omega \delta n_1 + n_0 \ii \bm{k} \cdot \bm{v}_1 &= 0, \label{eq:2014-07-09-4} \\
  - m \ii \omega \bm{v}_1 + \ii \bm{k}\left\{ g \delta n_1 + \frac{\hbar^2 k^2}{4 m n_0} \Delta \delta n_1 \right\} &= 0. \label{eq:2014-07-09-5}
\end{align}
%

\begin{notice}
  $\bm{v}_1 \parallel \bm{k}$ longitudinale Dichtewellen (Flüssigkeit.) 
\end{notice}

Ineinander einsetzen ergibt
%
\begin{align}
  \bm{v}_1 &= \ii \bm{k} \left( g+ \frac{\hbar^2 k^2}{4 mn_0}\right) \frac{\delta n_1}{n \ii \omega}.
\end{align}
%
Einsetzen in Gleichung~\eqref{eq:2014-07-09-4} ergibt
%
\begin{align*}
  0 &= \ii \omega \delta n_1 + n_0 \ii \bm{k} \cdot \ii \bm{k} \left(g+\frac{\hbar^2 k^2}{4 mn_0}\right) \frac{\delta n_1}{m \ii \omega} \quad \bigg|\cdot \ii \omega\\
  \omega^2 &= \frac{n_0 k^2}{m} \left( g+ \frac{\hbar^2 k^2}{4 m n_0} \right).
\end{align*}
%
Damit erhalten wir ebenfalls das \acct{Bogoliubov-Spektrum}
%
\begin{align}
  \boxed{ \omega^2 = k^2 \frac{gn_0}{m} + \frac{\hbar^2 k^2}{4m^2} }
\end{align}
%

Jetzt wollen wir zusätzlich noch die dipolare Wechselwirkung, wie sie bspw. in Abbildung~\ref{fig:2014-07-09-1} zu sehen ist berücksichtigen.
%
\begin{figure}[htp]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[MidnightBlue,->] (0,0) -- node[dot] (a) {} (0,2);
    \draw[MidnightBlue,->] (4,1) -- node[dot] (b) {} (4,3);
    \draw (a) -- node[below] {$\bm{r}$} (b);
    \draw[DarkOrange3,->] (0,1.5) to[out=20,in=90] node[above right] {$\theta$} ($(a)!.1!(b)$);
  \end{tikzpicture}
  \caption{Zur Veranschaulichung der dipolaren Wechselwirkung. Zwei Dipole deren Abstand durch $r$ und dem Winkel $\theta$ beschrieben sind.}
  \label{fig:2014-07-09-1}
\end{figure}
%
Der Wechselwirkungsterm 
%
\begin{align*}
  U_{dd} &= \frac{1- 3 \cos^2(\theta)}{r^3} \cdot \frac{C_{dd}}{4 \pi}
\end{align*} 
%
enthält den anisotropen Anteil $3\cos^2(\theta)$ und langreichweitige Wechselwirkung $1/r^3$. Je nach vorliegenden System nimmt hierbei $C_{dd}$ unterschiedliche Werte an 
%
\begin{align*}
  C_{dd} &=
  \begin{cases}
    \frac{d^2}{\varepsilon_0} , \text{el. dipolare Gase}\\
    \mu_0 \mu^2 , \text{magn. dipolare Gase}
  \end{cases}.
\end{align*}
%
D.h. die nichtlineare Schrödingergleichung (GP-Gleichung) wird um einen nichtlokalen Term ergänzt
%
\begin{align}
  \ii \hbar \partial_t \psi &= -\frac{\hbar^2}{2m}  \nabla^2 \psi + \bigg[ V_{ext} + \underbrace{g \vert \psi \vert^2}_{(\mathrm{a})} + \underbrace{\int \vert \psi(r',t)\vert^2 u_{dd}(r-r') \diff^3r'}_{(\mathrm{b})}  \bigg] \psi.
\end{align}
%
Term (a) beschreibt hierbei die Kontaktwechselwirkung mit $g= 4 \pi \hbar^2 a/m$ und Term (b) die dipolare Wechselwirkung.

Betrachten wir nun Bogoliubov mit dipolarer Wechselwirkung. Hierzu zunächst einen Identität aus der Elektrodynamik
%
\begin{align}
  \frac{1- 4 \frac{z^2}{r^2}}{r^3} &= - \frac{\partial^2}{\partial z^2} \frac{1}{r} - \frac{4 \pi }{3} \delta(r). \label{eq:2014-07-09-6}
\end{align}
%
Mit Hilfe von Gleichung~\ref{eq:2014-07-09-6} führt eine Fouriertransformation zu
% 
\begin{align*}
  \frac{4 \pi }{C_{dd}} \hat{u}_{dd}(k) &= k_z^2 \cdot \underbrace{\mathcal{F}\left(\frac{1}{r}\right)}_{4 \pi /k^2} - \frac{4 \pi}{3} \\
  \implies \hat{u}_{dd} &= C_{dd} \left[ \frac{k_z^2}{k^2} - \frac{1}{3} \right] \\
  &= C_{dd} \left[ \cos^2 \alpha - \frac{1}{3} \right].
\end{align*}
%
Die Bedeutung des Winkels $\alpha$ ist in Abbildung~\ref{fig:2014-07-09-2} zu sehen.
%
\begin{figure}[htp]
  \centering
  \begin{tikzpicture}[gfx]
    \foreach \x in {0,1.5,3} {
      \draw[MidnightBlue,->] (\x,0) -- (\x,2);
    }
    \draw[DarkOrange3,->] (0,0) -- node[left] {$k_z$} (0,1);
    \draw[DarkOrange3,->] (0,0) -- node[below right] {$k$} (1,1);
    \draw[dotted] (0,1) -- (1,1);
    \draw[DarkOrange3,->] (0,.8) arc (90:45:.8);
    \node[DarkOrange3] at (67.5:.6) {$\alpha$};
  \end{tikzpicture}
  \caption{Zur Veranschaulichung der Fouriertransformation und des Winkels $\alpha$. Es zeigt sich $k_z = k \cos \alpha$.}
  \label{fig:2014-07-09-2}
\end{figure}
%

Im Zusammenhang mit Bogoliubov gilt
%
\begin{align*}
  g n(r,t) &\to gn(r,t) + \int n(r',t) u_{dd}(r-r') \diff^3r'.
\end{align*}
%
D.h. wir erhalten 
%
\begin{align}
  \boxed{ \omega^2 = k^2 \left[ \frac{n_0}{m} \left(g+C_{dd} (\cos^2\alpha- 1/3)\right)+ \frac{\hbar^2 k^2}{2m^2} \right] }
\end{align}
%
Es zeigt sich, falls $C_{dd} > 3 g$ und $k\to 0$ wird $\omega^2 <0$, d.h. es tritt eine Instabilität auf (Phononen induzierter Kollaps). Siehe hierzu die Dispersionsrelation in Abbildung~\ref{fig:2014-07-09-3}.
%
\begin{figure}[htp]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[<->] (0,3) node[right] {$\omega^2$} |- (4,0) node[below] {$k$};
    \draw[MidnightBlue] (0,0) to[out=0,in=-100] node[left,pos=.8] {$\alpha=0$} (3,3);
    \draw[MidnightBlue] (0,0) to[out=-45,in=-100] node[right,pos=.6] {$\alpha=\pi/2$} (4,3);
    \clip (0,0) to[out=-45,in=-100] (4,3) -- cycle;
    \fill[pattern=north east lines] (0,0) rectangle (4,-1);
  \end{tikzpicture}
  \caption{Dispersionsrelation. Der schraffierte Bereich ist für die Instabilität verantwortlich.}
  \label{fig:2014-07-09-3}
\end{figure}
%

\begin{notice}
  Allgemein geht die Fouriertransformation einer 2-Teilchen Wechselwirkung in die Bogoliubov Dispersionsrelation über (siehe Abbildung~\ref{fig:2014-07-09-4}).
\end{notice}

\begin{figure}[htp]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}
      \draw[<->] (0,2) node[left] {$U(r)$} |- (1,0) |- (2,1) node[below] {$r$};
      \node[above] at (1,1) {$r_0$};
    \end{scope}
    \node at (3,1) {$\implies$};
    \begin{scope}[shift={(4,0)},looseness=.7]
      \draw[<->] (0,2) node[left] {$\omega$} |- (2,0) node[below] {$k$};
      \draw (0,0) to[out=60,in=180] (.75,1) to[out=0,in=180] (1.2,.5) to[out=0,in=-100] coordinate[pos=.3] (A) (2,2);
      \draw (1.2,2pt) -- (1.2,-2pt) node[below] {$2\pi/r_0$};
    \end{scope}
    \node[pin={right:Roton}] at (A) {};
  \end{tikzpicture}
  \caption{Bogoliubov Dispersionsrelation.}
  \label{fig:2014-07-09-4}
\end{figure}
