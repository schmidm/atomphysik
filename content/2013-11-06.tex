% Vorlesung am 06.11.2013
% TeX: Michi

\renewcommand{\printfile}{2013-11-06}

\begin{notice*}
Für das quantisierte elektrische und magnetische Feld in einer Dimension gilt
%
  \begin{align*}
    \hat{E}_x (z,t) &= \mathcal{E}_0 \left(\hat{a}+\hat{a}^\dagger\right) \sin(kz) \\
    \hat{B}_y (z,t) &= \mathcal{B}_0 \left(\hat{a}-\hat{a}^\dagger\right) \cos(kz) 
  \end{align*}
% 
wobei für $\mathcal{E}_0$ und $\mathcal{B}_0$ 
%
\begin{align*}
  \mathcal{E}_0 &= \sqrt{\frac{\hbar \omega}{\varepsilon_0 V}} \\
  \mathcal{B}_0 &= \left(\frac{N_0}{k}\right) \sqrt{\frac{\varepsilon_0 \hbar \omega^2}{V}}
\end{align*}
%
gilt. $\mathcal{E}_0$ und $\mathcal{B}_0$ können hierbei als elektrisches und magnetisches Feld pro Photon interpretiert werden.
\end{notice*}

Der Hamiltonoperator lässt sich dann schreiben als
\[
  \hat{H} = \hbar\omega \left(\hat{a}^\dagger\hat{a} + \frac{1}{2}\right).
\]
Wobei die Anzahlzustände, bzw \acct{Fockzustände} $\ket{n}$ als Anzahl der Photonen in einer Mode interpretiert werden können. 

\begin{theorem}
Der \acct{Anzahloperator} ist wie folgt definiert:
%
\begin{align}
  \boxed{\hat{n} \equiv \hat{a}^\dagger \hat{a}} \label{eq: 06.11-1}
\end{align}
%
\end{theorem} 

Mit \autoref{eq: 06.11-1} vereinfacht sich der Hamiltonoperator zu
\[
  \hat{H} = \hbar \omega \left(\hat{n} + \frac{1}{2}\right).
\] 
Für die Schrödingergeichung bedeutet dies
%
\begin{align*}
  \hat{H} \ket{n} &= E \ket{n} \\
  \implies E_n &= \hbar \omega \left(n + \frac{1}{2}\right) \qquad \text{und} \qquad n \in \mathbb{N}
\end{align*}
%
D.h. dass das Problem auf den quantenmechanischen harmonischen Oszillator reduziert wurde, dessen Lösung wir bereit aus der Quantenmechanik kennen, bzw. in Abbildung~\ref{fig:2013-11-06-1} zu sehen ist.
%
\begin{figure}[htpb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[MidnightBlue] plot[domain=-1.8:1.8] (\x,{\x*\x});
    \draw[DarkOrange3] (-2,0.5) -- (2,0.5) node[right] (n0) {$n=0$ \quad $\frac{1}{2} \hbar \omega$};
    \draw[DarkOrange3] (-2,1.5) -- (2,1.5) node[right] (n1) {$n=1$ \quad $\frac{3}{2} \hbar \omega$};
    \draw[DarkOrange3] (-2,2.5) -- (2,2.5) node[right] (n2) {$n=2$ \quad $\frac{5}{2} \hbar \omega$};
    \draw[Purple,pattern color=Purple,pattern=north east lines] plot[domain=-2:2] (\x,{0.5*exp(-(\x*\x)/0.5)+0.5});
    \draw (-0.2,1.6) edge[->,bend left] node[left] {$a^\dag$} (-0.2,2.4);
    \draw (0.2,2.4) edge[->,bend left] node[right] {$a$} (0.2,1.6);
  \end{tikzpicture}
  \caption{Lösungen des harmonischen Oszillator}
  \label{fig:2013-11-06-1}
\end{figure}
%
\begin{theorem}
Für die Leiteroperatoren gilt
%
\begin{align*}
  \hat{a} \ket{n} &= \sqrt{n} \ket{n-1} \\
  \hat{a}^\dagger \ket{n} &= \sqrt{n+1} \ket{n+1}.
\end{align*}
%
\end{theorem}

\subsection{Quantenfluktuationen}

Betrachten wir zunächst den Erwartungswert von $\hat{E}_x$ aus vorherigen Kapitel.
%
\begin{align*}
  \Braket{\hat{E}_x} &= \Braket{n|\hat{E}_x|n} \\
  &= \mathcal{E}_0 \sin(kz) \bigg[\underbrace{\Braket{n|\hat{a}^\dagger|n}}_{=0} + \underbrace{\Braket{n|\hat{a}|n}}_{=0}\bigg] \\
  &= 0
\end{align*}
%
Die Fluktuationen des elektrischen Feldes sind durch die \acct{Varianz} gegeben. für diese gilt
%
\begin{align*}
  \Delta \hat{E}_x &= \sqrt{\Braket{\hat{E}^2_x}-\underbrace{\Braket{\hat{E}_x}^2}_{=0}} \\
  &= \sqrt{\mathcal{E}_0^2 \sin^2(kz) \Braket{ \left(\hat{a}^\dagger\right)^2+\left(\hat{a}\right)^2+ \hat{a}\hat{a}^\dagger + \hat{a}^\dagger \hat{a}} } \\
  &= \sqrt{\mathcal{E}_0^2 \sin^2(kz) \Braket{\hat{a}\hat{a}^\dagger + \hat{a}^\dagger \hat{a}}}\\
  &= \sqrt{\mathcal{E}_0^2 \sin^2(kz) \Braket{2\hat{a}^\dagger\hat{a} + 1}} \\
  &= \sqrt{ 2 \mathcal{E}_0^2 \sin^2(kz) \Braket{\hat{n} + \frac{1}{2}}} \\
  &= \sqrt{2} \mathcal{E}_0 \sin(kz) \sqrt{n+\frac{1}{2}}
\end{align*}
%
\begin{example*}
  Wir betrachten eine Box mit \SI{1}{\milli\m} Kantenlänge mit Vakuum ($N=0$), so gilt für die Grundmoden bei verschiedenen Wellenlängen
  %
  \begin{align*}
    \lambda &= \SI{2}{\milli\m} \implies \mathcal{E}_0 \approx \SI{0.15}{\volt\per\centi\m} \\
    \lambda &= \SI{500}{\milli\m} \implies \mathcal{E}_0 \approx \SI{10}{\volt\per\centi\m}
  \end{align*}
  %
\end{example*}

\subsection{Multimoden Felder und Zustandsdichte}
Wir betrachten die verschiedenen Moden in einer Box, wie in Abbildung~\ref{fig:2013-11-06-2}, für diese gilt
%
\begin{align*}
  k_x &= \left(\frac{2 \pi }{L}\right)m_x \\
  k_y &= \left(\frac{2 \pi }{L}\right)m_y \\
  k_z &= \left(\frac{2 \pi }{L}\right)m_z 
\end{align*}
%
\begin{figure}[htpb]
  \centering
  \tdplotsetmaincoords{75}{140}
  \begin{tikzpicture}[gfx,tdplot_main_coords]
    \draw[dashed] (0,0,0) -- (3,0,0) (0,3,0) -- (0,0,0) -- (0,0,3); 
    \draw (3,0,0) -- node[below] {$L$} (3,3,0) (3,3,0) -- (0,3,0);
    \draw (0,0,3) -- (3,0,3) -- (3,3,3) -- (0,3,3) -- cycle;
    \draw (3,0,0) -- (3,0,3) (3,3,0) -- (3,3,3) (0,3,0) -- (0,3,3);
    \tdplotsetrotatedcoords{90}{90}{90}
    \coordinate (Shift) at (3,3,0);
    \tdplotsetrotatedcoordsorigin{(Shift)}
    \draw[MidnightBlue,tdplot_rotated_coords] plot[smooth,samples=91,domain=0:3] (\x,{sin(2*pi*deg(\x))^2});
    \tdplotsetrotatedcoords{180}{90}{90}
    \coordinate (Shift) at (3,3,0);
    \tdplotsetrotatedcoordsorigin{(Shift)}
    \draw[DarkOrange3,tdplot_rotated_coords] plot[smooth,samples=61,domain=0:3] (\x,{sin(pi*deg(\x))^2});
    \tdplotsetrotatedcoords{0}{90}{0}
    \coordinate (Shift) at (3,0,3);
    \tdplotsetrotatedcoordsorigin{(Shift)}
    \draw[Purple,tdplot_rotated_coords] plot[smooth,domain=0:3] (\x,{sin(pi/3*deg(\x))^2});
  \end{tikzpicture}
  \caption{Moden in einer Box mit Kantenlänge $L$}
  \label{fig:2013-11-06-2}
\end{figure}
%
Für die Wellenzahl bedeutet dies
\[
  \bm{k} = \frac{2 \pi}{L} \begin{pmatrix}m_x \\ m_y \\ m_z\end{pmatrix}
\]
Für die Anzahl der Moden im Intervall ergibt sich:
%
\begin{align*}
  \Delta m &= \Delta m_x \cdot \Delta m_y \cdot \Delta m_z \\
  &= 2 \left(\frac{L}{2\pi}\right)^3 \Delta  k_x \cdot \Delta k_y \cdot \Delta k_z
\end{align*}
%
Der Faktor $2$ rührt daher, dass 2 unabhängige Polarisationen existieren.

\begin{notice}[Frage:]
  Wie viele Moden liegen in der Schale $|\bm{k}| + \diff k$ bzw. $\omega + \diff \omega$ wobei $\omega = ck$ ?
\end{notice}
%
\begin{figure}[htpb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (-3,0) -- (3,0) node[below] {$k_x$};
    \draw[->] (0,-2) -- (0,2) node[right] {$k_y$};
    \foreach \x in {-1.5,-1,...,1.5} {
      \foreach \y in {-1.5,-1,...,1.5} {
        \node[dot] at (\x,\y) {}; 
      };
    };
    \draw[DarkOrange3] (0,0) circle [radius=1];
    \draw[DarkOrange3] (0,0) circle [radius=1.5];
    \draw[DarkOrange3] (0,0) -- node[below right=-0.1] {$k$} (45:1);
    \draw[DarkOrange3] (45:1) -- node[below right=-0.1] {$\diff k$} (45:1.5);
  \end{tikzpicture}
  \caption{Zweidimensionale Moden in einem infinitesimalen Kreissegment $\diff k$}
  \label{fig:2013-11-06-3}
\end{figure}
%
Für die Beantwortung der Frage betrachten wir Abbildung~\ref{fig:2013-11-06-3} wobei wir annehmen, dass $\lambda \ll L$, so dass für das wir folgenden Differentialoperator erhalten
%
\begin{align*}
  \diff m &= 2 \frac{V}{8 \pi^3} \diff x \diff y \diff z \\
  &=  2 \frac{V}{8 \pi^3} k^2 \diff k \underbrace{\sin(\theta) \diff \theta \diff \phi}_{= 4 \pi } \\
  &= \frac{2 V}{8 \pi^3} 4 \pi k^2 \diff k 
\end{align*}
%
Damit ergibt sich die \acct{Modendichte} zu
%
\begin{align}
  \varrho(k) \diff k &= \frac{k^2}{\pi^2} \diff k \\
  \varrho(k) \diff k &= \frac{\omega^2}{\pi^2 c^3} \diff \omega
\end{align}
%

\section{Lambshift}
In Abbildung~\ref{fig:2013-11-06-4} ist die Lambaufspaltung des Wasserstoffatoms angedeutet, eine zusätzliche Verschiebung der Energieniveaus. 
%
\begin{figure}[htpb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw (0,0) node[left] {$n=1$} -- (2,0);
    \draw[dashed] (2,0) -- (3,-1);
    \draw (3,-1) -- node[above] {$1 S_{1/2}$} node[below] {\enquote{LS}} (5,-1);
    \draw[dashed] (5,-1) -- (6,-0.5);
    \draw[dashed] (5,-1) -- node[below] {\enquote{Lamb-Shift}} (7,-1);
    \draw (6,-0.5) -- (7,-0.5);
    \node[right] at (7,-0.75) {\SI{8.17}{\giga\Hz}};
  \end{tikzpicture}
  \caption{Lambaufspaltung des $1S_{1/2}$ Zustands des Wasserstoffatoms}
  \label{fig:2013-11-06-4}
\end{figure}
%
Ursachen des \acct*{Lambshifts} \index{Lambshift}:
\begin{enumerate}
  \item Aus der spontanen Paarbildung von Elektronen und Positronen folgt die Quantisierung des Vakuums. Vgl. Abbildung~\ref{fig:2013-11-06-5}.
  %
  \begin{figure}[htpb]
    \centering
    \begin{tikzpicture}[
      gfx,
      photon/.style={MidnightBlue,decoration={snake},decorate},
      gluon/.style={Purple,decoration={coil,amplitude=4pt,segment length=5pt},decorate},
      fermion/.style={DarkOrange3,arrow inside}
    ]
      \draw[photon] (0,0) -- node[above=1ex] {$h \cdot \nu$} (2,0);
      \draw (2,0) edge[fermion,bend left=90,looseness=1.75] node[above] {$e^-$} (3,0);
      \draw (3,0) edge[fermion,bend left=90,looseness=1.75] node[below] {$e^+$} (2,0);
      \draw[photon] (3,0) -- node[above=1ex] {$h \cdot \nu$} (5,0);
      \node[dot] at (2,0) {};
      \node[dot] at (3,0) {};
    \end{tikzpicture}
    \caption{Spontane Paarbildung im Feynmandiagramm}
    \label{fig:2013-11-06-5}
  \end{figure}
  %
  \item Vakuumfluktuationen des elektrischen Feldes induzieren eine Oszillation des Elektrons (keine Zitterbewegung!). Vgl. Abbildung~\ref{fig:2013-11-06-6}.
  %
  \begin{figure}[htpb]
    \centering
    \begin{tikzpicture}[gfx]
      \draw plot[domain=-3:-0.6] (\x,{-1/((\x)^2)});
      \draw plot[domain=0.6:3] (\x,{-1/((\x)^2)});
      \draw[Purple,rounded corners=8] (-3,0) .. controls (-1.3,0) .. (0,-2) .. controls (1.3,0) ..(3,0);
      \node[MidnightBlue,dot] (e) at (-2,{-1/((2)^2)}) {};
      \draw[MidnightBlue] (e) -- +(-60:1.1) -- +(-60:-1.1);
      \draw[MidnightBlue,decoration={snake},decorate] (e) -- +(-60:1);
      \draw[MidnightBlue,decoration={snake},decorate] (e) -- +(-60:-1) node[above] {$\omega = c \cdot k$};
      \draw[->,DarkOrange3] (e) -- +(30:-1);
      \draw[->,DarkOrange3] (e) -- +(30:1) node[above] {$\delta r$};
      \draw[Purple] (0.4,-1.6) edge[<-,bend right] (1.5,-1.5);
      \node[Purple,right,text width=4.1cm] at (1.5,-1.5) {Das Elektron sieht nur ein verschmiertes Potential.};
    \end{tikzpicture}
    \caption{Veranschaulichung der Vakuumfluktuationen}
    \label{fig:2013-11-06-6}
  \end{figure}
  %
\end{enumerate}

Abschätzung der Energieverschiebung $\braket{\delta V}$ durch Oszillationen nach Welton (Phys. Rev. 74, 1157 (1948)). Wir unterziehen $V$ einer Taylorentwicklung
\[
  \delta V = \sum_{i} \frac{\partial V}{\partial x_i} \delta x_i + \sum_{i,k} \frac{1}{2} \frac{\partial^2 V}{\partial x_i \partial x_k} \delta x_i \delta x_k.
\]
Da die Fluktuationen isotrop sind, verschwindet das zeitliche Mittel
\[
  \braket{\delta x}_t = 0
\]
und unabhängig bezüglich der Richtung ist
\[
  \braket{\delta x_i \delta x_k}_t = 0 \qquad \text{für} \qquad i \neq k.
\]
Weiterhin gilt:
\[
  \braket{\delta \bm{r}^2}_t = 3 \braket{\delta x}.
\]
Für $\braket{\delta V}$ bedeutet dies
\[
  \braket{\delta V} = \frac{1}{6} \braket{\Delta V(r) } \braket{\delta r^2}.
\]
Wir betrachten nun $\braket{\Delta V(r)}_t$
%
\begin{align*}
  \braket{\Delta V(r)} &= \Braket{n, \ell, m|\Delta\left(-\frac{e^2}{4 \pi \varepsilon_0} \frac{1}{r}\right)|n, \ell, m} \\
  &= \left(-\frac{e^2}{\varepsilon_0}\right) \Braket{n, \ell, m|\delta(r)|n, \ell, m} \\
  &= \frac{e^2}{\varepsilon_0} \frac{1}{\pi a_0^3} \frac{Z^4}{n^3} \quad \text{für} \quad \ell = 0
\end{align*}
%
Die Deltadistribution $\delta(r)$ zeigt, dass der größte Effekt am Ursprung auftritt. Es folgt für den zweiten Faktor
%
\begin{align*}
  \Braket{\delta \bm{r}^2} &= \sum_{k} \Braket{\delta \bm{r}_k^2}
\end{align*}
%
wobei $\omega_k = c \cdot k$ ist. Wir bedienen uns folgendem Ansatz:
%
\begin{align*}
  m \delta\ddot{\bm{r}}_k &= e \bm{E}_k \, \ee^{\ii \omega_k t} \\
  \implies \delta \bm{r}_k &= - \frac{e}{m} \frac{\bm{E}_k}{\omega_k^2} \, \ee^{\ii \omega_k t}
\end{align*}
%
Somit ergibt sich
%
\begin{align*}
  \Braket{\delta \bm{r}^2} &= \sum_{k} \frac{e^2}{\omega ^4 m^2} \Braket{\bm{E}_k^2} \\
  &= 2 \frac{V}{8 \pi^3} \int \frac{e^2}{m^2 c^4 k^4} \Braket{\bm{E}_k^2} \underbrace{\diff^3 k}_{k^2 \diff k \cdot 4 \pi}
\end{align*}
%
Mit 
%
\begin{align*}
  \Braket{\bm{E}_k} &= \Braket{\mathcal{E}_0^2 \sin^2(kz)} \\  
  &= \frac{\hbar k c}{V \varepsilon_0} \frac{1}{2}
\end{align*}
%
ergibt sich
%
\begin{align}
  \Braket{\delta \bm{r}^2} &= \frac{1}{2} \frac{1}{\pi^2} \frac{e^2}{m^2 c^3} \frac{\hbar}{\varepsilon_0} \int_{0}^{\infty} \frac{1}{k} \diff k \label{eq: 6.11-2}
\end{align}
%
Diese Integral divergiert aber bezüglich der Integrationsgrenzen, d.h. es muss sowohl für die obere als auch untere grenzen eine Grenze gefunden werden, so dass eine sinnvolle Lösung gefunden werden kann. 
\begin{description}
  \item[Untere Grenze:] Die niedrigste Energie ist durch den Abstand zum nächsten Energieniveau gegeben.
  %
  \begin{align*}
    E_{\mathrm{Ryd}} &= \frac{me^4Z^2}{(4 \pi \varepsilon_0)^2 2 \hbar^2} \\
    k_{\mathrm{min}} &= \frac{E_{\mathrm{Ryd}} \eta}{\hbar c} \quad \text{mit} \quad \eta = \left(\frac{1}{n^2} - \frac{1}{(n+1)^2}\right)
  \end{align*}
  % 
  \item[Obere Grenze:] Bewegung des Elektrons wird durch die relativistische Massenzunahme gehemmt $(\hbar \omega_{\mathrm{max}} \approx m c^2)$:
  %
  \begin{align*}
    k_{\mathrm{max}} &= \frac{mc^2}{\hbar c} = \frac{mc}{\hbar}
  \end{align*}
  % 
\end{description}
Somit folgt für \autoref{eq: 6.11-2}
%
\begin{align*}
  \int_{k_{\mathrm{min}}}^{k_{\mathrm{max}}} \frac{1}{k} \diff k 
  &= 2 \ln\left(\frac{c 4 \pi \varepsilon_0 \hbar}{e^2 Z} \sqrt{\frac{2}{\eta}}\right).
\end{align*}
%
Für $\braket{V}$ bedeutet dies
%
\begin{align*}
  \braket{\delta V} = \frac{1}{6} \frac{e^4 \hbar}{\varepsilon_0^2 \pi^3 a_0^3 m^2 c^3}\frac{Z^4}{n^3} \ln\left(\frac{c 4 \pi \varepsilon_0 \hbar}{e^2 Z} \sqrt{\frac{2}{\eta}}\right).
\end{align*}
%
Hierbei ist der Term $Z^4 /n^3$ dominant da der Logarithmus mit $\sim \ln(n/Z)$ abfällt, bzw. ansteigt. 

In \autoref{tab: 06.11-1} sind verschiedene Zustände und deren exakte sowie mit der obigen Rechnung gefundenen Werte zu sehen.
%
\begin{table}[htpb]
  \centering
  \begin{tabular}{lSS}
    \toprule
    Zustand  & {exakt [\si{\giga\Hz}]} & {Rechnung [\si{\giga\Hz}]} \\
    \midrule
    $1 S_{1/2}$ & 8.17  & 11.7  \\
    $2 S_{1/2}$ & 1.057 & 1.69  \\
    $2 P_{1/2}$ & -14   & 0     \\
    $3 S_{1/2}$ & 0.313 & 0.544 \\
    \bottomrule
  \end{tabular}
  \caption{Vergleich exakter und durch die Rechnung ermittelten Werte der Lambverschiebung für verschiedene Zustände des Wasserstoffatoms.}
  \label{tab: 06.11-1}
\end{table}
%
