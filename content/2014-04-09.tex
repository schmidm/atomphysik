% Vorlesung am 09.04.2014
% TeX: Michi

\renewcommand{\printfile}{2014-04-09}

\subsection{Die Dopplerkühlung}
Im Folgenden betrachten wir zwei-Niveau-Atome zwischen zwei entgegengesetzt gerichteten Laserstrahlen. Das Atom emittiert hierbei spontan Photonen in alle Raumrichtungen und bewege sich mit einer Geschwindigkeit $\bm{v}$ nach links, siehe hierzu Abbildung~\ref{fig:2014-04-09-1}. Bei der \acct{Dopplerkühlung} nutzen wir den Dopplereffekt des bewegten Atoms sowie die Verstimmung der Lasers aus.
%
\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[DimGray] (-0.2,0.1) -- (0.2,0.1);
    \draw[DimGray] (-0.2,-0.1) -- (0.2,-0.1);
    \node[draw,circle,MidnightBlue,inner sep=0.3cm] (twolevel) at (0,0) {};
    \foreach \i in {45,135,...,325} {
      \draw[->,MidnightBlue,decorate,decoration={snake,post length=2mm}] (twolevel) -- ++(\i:1.5);
    }
    \draw[->] (twolevel) -- node[above] {$\bm{v}$} ++(-1.5,0);
    \node[draw,DarkOrange3,single arrow,label={93:Laser}] at (-3,0) {$\delta < 0$};
    \node[draw,DarkOrange3,single arrow,shape border rotate=180] at (3,0) {\phantom{$\delta < 0$}};
  \end{tikzpicture}
  \caption{Zwei Niveau Atom zwischen zwei entgegengesetzt gerichteten Lasern mit der Verstimmung $\delta <0$.}
  \label{fig:2014-04-09-1}
\end{figure} 
%
Die Laser haben hierbei ein Geschwindigkeitsprofil wie in Abbildung~\ref{fig:2014-04-09-2}. Die spontane Lichtkraft ist gegeben durch
%
\begin{align*}
  \bm{F} & = \Gamma \hbar \bm{k} \propto P_e.
\end{align*}
%
$P_e$ entspricht hierbei der Wahrscheinlichkeit im angeregten Zustand zu sein und $\bm{k}$ dem Wellenvektor des Lasers.
%
\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}[gfx,/pgf/number format/1000 sep={\,}]
    \begin{axis}[
        gfx,
        width=0.65\textwidth,
        height=6cm,
        no markers,
        samples=100,
        smooth,
        enlargelimits=false,
        restrict y to domain=-0.5:0.5,
        legend style={draw=none,fill=none},
        xlabel = {$kv$},
        ylabel = {$F$}
      ]
      \addplot+[MidnightBlue] {0.3/(1+(x+1)^2)};
      \addlegendentry{$F_+$};
      \addplot+[DarkOrange3] {-0.3/(1+(x-1)^2)};
      \addlegendentry{$F_-$};
      \addplot+[Purple] {0.3/(1+(x+1)^2)-0.3/(1+(x-1)^2)};
      \addlegendentry{$F_\textnormal{ges}$};
      \addplot+[DimGray] {-0.3*x};
      \addlegendentry{$F = - \beta v$};
    \end{axis}
  \end{tikzpicture}
  \caption{Die spontane Lichtkraft der beiden Laser über $kv$ aufgetragen. Die Laser weisen hierbei eine typisches Lorentzkurve auf, sowie die Überlagerung beider Kurven zu einer resultierenden mit linearer Näherung im Ursprung. Die am linken Maximum bestimmte Geschwindigkeit $v_{\mathrm{einfang}}$ wird Einfanggeschwindigkeit genannt dessen Größe bei $k v_{\mathrm{einfang}} = \Gamma/2$ liegt.}
  \label{fig:2014-04-09-2}
\end{figure}
%
Die spontane Lichtkraft eines Lasers ist demnach
%
\begin{align*}
  \bm{F} &\propto P_e \propto S(\delta) = \frac{\omega_R^2/2}{\delta^2 + \Gamma^2/4}.
\end{align*}
%
Im Folgenden betrachten wir die Näherungen für $S \ll 1$ sowie $v \neq 0$, also bewegten Atomen und die Kraft für ein einziges Lichtfeld (linker Laser).
%
\begin{align*}
  \bm{F} &= \frac{\Gamma}{2} \frac{S}{S+1} \\
  &\stackrel{S\ll1}{\approx} \frac{\Gamma}{2} S(\delta) \hbar k \\
  &\stackrel{v\neq0}{\approx} \frac{\Gamma}{2} S(\delta -kv) \hbar k.
\end{align*}
%
Unter Berücksichtigung des anderen Lasers erhalten wir für die Gesamtkraft
%
\begin{align}
  \bm{F}_{\mathrm{ges}} &= \frac{\hbar k \Gamma}{2} \left[ S(\delta - kv) - S(\delta + kv) \right].
\end{align}
%
Linearisieren wir die Kraft im Ursprung, also um $v = 0$ erhalten ein Kraft der Form
%
\begin{align*}
  F &= - \alpha v.
\end{align*}
%
Hierbei entspricht $\alpha$ einem Reibungskoeffizienten, wie wir ihn aus einem Dämpfungsterm der klassischen Mechanik her kennen. Eine kurze Rechnung zeigt, dass
%
\begin{align*}
  \alpha &= - \frac{\hbar k^2 \delta \Gamma \omega_R^2}{\delta^2 + \Gamma^2/4}.
\end{align*}
%
Fluktuationen zu einem dissipativen Term führen, der noch vernachlässigt wurde. Dieser kommt aus der Diffusion der Atome (Random walk). Unter Berücksichtigung besagten Terms gilt für den Impuls
%
\begin{align}
  \braket{p(t)} &= 0\\
  \braket{p^2(t)} &\neq 0.
\end{align}
%
D.h. der Impulsmittelwert verschwindet, nicht jedoch der quadratische Impulsmittelwert. Bei der Diffusion gilt nach dem typischen random walk
%
\begin{align}
  \braket{p^2(t)} &= 2 D_p t,
\end{align}
% 
wobei $D_p$ die Diffusionskonstante im Impulsraum ist. 
%
\begin{notice}
  Es gilt 
  %
  \begin{align*}
    k_\mathrm{B} T &= \frac{1}{2} m \braket{v^2} \\
    \Delta p &= \hbar k \\
    \implies \Delta V &= \frac{\Delta p^2 }{2 m h} = \frac{\hbar^2 k^2}{2 m h}.
  \end{align*}
  %
  $\Delta p$ entspricht dem Impulsunterschied zwischen absorbierten und emittierten Photonen, d.h. emittierte Photonen haben eine unterschiedliche Frequenz als absorbierte.
\end{notice}
%
Demnach gilt
%
\begin{align*}
  \braket{p^2(t)} &= \underbrace{\left(\hbar k\right)^2 \Gamma_{\mathrm{sc}}(v=0) \left(1+ \xi\right)}_{2 D_p} \cdot t.
\end{align*}
%
Die Terme der Klammer $(1+\xi)$ haben hierbei die folgende Bedeutung:
\begin{itemize}
  \item Die $1$ entspricht hierbei der statistischen Verteilung der absorbierten Photonen.
  \item  Das $\xi$ entspricht hierbei der statistischen Verteilung der emittierten Photonen. In einer Dimension ist $\xi = 1$. Für linear polarisiertes Licht in drei Dimensionen wird $\xi = 2/5$. 
\end{itemize}
%
$\Gamma_{\mathrm{sc}}$ ist in diesem Kontext die Streurate (sc $\hateq$ scattering). Im Gleichgewicht gilt
%
\begin{align*}
  k_\mathrm{B} T &= \frac{D_p}{\alpha} = \frac{1+\xi}{8} \hbar \Gamma \left[ \frac{2 \delta}{\Gamma} + \frac{\Gamma}{2 \delta}\left( 1+ \frac{2 I }{I_{\mathrm{sat}}} \right) \right].
\end{align*}
%
Die $I$ sind hierbei Intensitäten. Für kleine Intensitäten $I \ll I_\mathrm{sat}$ folgt
%
\begin{align}
  \boxed{k_\mathrm{B} T_D \bigg|_{\mathrm{min}} = \frac{\hbar \Gamma}{2} \left(\frac{1+ \xi}{2}\right) } \quad \text{für} \quad \delta = -\frac{\Gamma}{2}. \label{eq:9.04-1}
\end{align}
%
Es zeigt sich, dass Gleichung~\eqref{eq:9.04-1} unabhängig von Größen wie $m$, $k$, $\lambda$, \ldots ist, sondern nur von $\Gamma$ abhängt. Tabelle~\ref{tab:9.4-1} gibt hier verschiedene \acct*{Dopplertemperaturen} \index{Dopplertemperatur} $T_D$ an.
%
\begin{table}[ht]
  \centering
  \begin{tabular}{Sc}
    \toprule
    {$T_D$}                 & Elemente    \\
    \midrule
    \SI{240}{\micro\kelvin} & Na          \\
    \SI{38}{\micro\kelvin} & He${}^\ast$ \\
    \SI{120}{\micro\kelvin} & Cr, Rb      \\
    \bottomrule
  \end{tabular}
  \caption{Doppertemperaturen verschiedener Elemente.}
  \label{tab:9.4-1}
\end{table}
% 
\begin{notice}
  Die Dopplertemperatur ist in der Größenordnung von $\sim \SI{100}{\micro\kelvin}$.
\end{notice}
%
Der Vergleich mit experimentellen Messungen zeigt, dass die erreichten Temperaturen geringer als der theoretische Wert ist, d.h. das Experiment besser als die Theorie ist. Grund dafür sind zusätzliche Interferenzeffekte die im Versuch auftreten und die Kühlung unterstützen. Zur Bestimmung der Temperatur müssen immer zwei Kurven (Verteilungen) aufgenommen werden, die Anfangs- und die Endkurve. Nur so lässt sich die Kühlung messen. Eine ausführliche theoretische Behandlung der Dopplerkühlung ist im Anhang zu finden.

\subsection{Magnetooptische Falle}
Im Folgenden wollen wir einen $s$- und $p$-Zustand eines Atoms, wie in Abbildung~\ref{fig:2014-04-09-3} betrachten. Wobei eine Aufspaltung aufgrund eines Magnetfeldes zu sehen ist. Wieder lässt sich die spontane Lichtkraft um $r=0$ linearisieren, wobei wir eine Kraft der Form
%
\begin{align}
  \bm{F} &= - \gamma \bm{r},
\end{align}
%  
erhalten. $\gamma$ ist das Analogon zu einer Federkonstante mit 
%
\begin{align*}
  \gamma &= \alpha \frac{g \mu_B \frac{\partial B}{\partial z}}{\hbar k}.
\end{align*}
%
\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}[shift={(-4.0,0)}]
      \draw (-0.5,0.5) -- (0.5,0.5) node[right] {$p$};
      \draw (-0.5,-0.5) -- (0.5,-0.5) node[right] {$s$};
    \end{scope}
    \begin{scope}
      \draw[->] (-2.5,-1.2) -- (2.5,-1.2) node[below] {$r$};
      \draw (0,-1.1) -- (0,-1.3) node[below] {$0$};
      \draw (-2,-1) -- (2,-1);
      \draw (-2,1) -- (2,1) node[right] {$m=0$};
      \draw (-2,1.5) -- (2,0.5) node[right] {$m=-1$};
      \draw (-2,0.5) -- (2,1.5) node[right] {$m=+1$};
      \node[dot] at (0,1) {};
      \draw[MidnightBlue,dashed,-|] (-1,-1) -- (-1,0.75);
      \draw[MidnightBlue,<->|] (0,-1) -- (0,0.75);
      \draw[MidnightBlue,dashed,-|] (1,-1) -- (1,0.75);
      \draw[->,DarkOrange3,decorate,decoration={snake,post length=2mm}] (-3,0) -- node[below] {$\sigma_+$} (-1.5,0);
      \draw[->,DarkOrange3,decorate,decoration={snake,post length=2mm}] (3,0) -- node[below] {$\sigma_-$} (1.5,0);
    \end{scope}
    \begin{scope}[shift={(5.5,0)}]
      \draw[->] (-2,0) -- (2,0) node[below] {$r$};
      \draw[->] (0,-1.5) -- (0,1.5) node[right] {$F$};
      \draw[MidnightBlue,smooth,domain=-2:2] plot (\x,{1/(1+(\x+1)^2)-1/(1+(\x-1)^2)});
      \draw[|-|] (135:0.5) -- (-45:0.5);
      \draw[dotted] (135:0.5) -- (135:0.5 |- 0,0);
      \draw[dotted] (-45:0.5) -- (-45:0.5 |- 0,0);
      \node[above right] at (0,0) {linear um $0$};
    \end{scope}
  \end{tikzpicture}
  \caption{Aufgespaltetes $s$ und $p$ Niveau im Magnetfeld mit der spontanen Lichtkraft über den Ort aufgetragen, um den Vergleich zu Abbildung~\ref{fig:2014-04-09-2} zu bemerken.}
  \label{fig:2014-04-09-3}
\end{figure}
%
Abbildung~\ref{fig:2014-04-09-4} zeigt die schematische Anordnung in drei Dimensionen.
%
\begin{figure}[htbp]
  \centering
  \tdplotsetmaincoords{80}{30}
  \begin{tikzpicture}[gfx]
    \shade[ball color=DimGray] (0,0,0) circle (0.1);
    \begin{scope}[tdplot_main_coords]
      \draw (0,0,1) circle (1);
      \draw (0,0,-1) circle (1);
      \draw[<-] (1.5,0,0) -- (0.5,0,0);
      \draw[<-] (-1.5,0,0) -- (-0.5,0,0);
      \draw[->] (0,1.5,0) -- (0,0.5,0);
      \draw[->] (0,-1.5,0) -- (0,-0.5,0);
      \draw[->] (0,0,1.5) -- (0,0,0.5);
      \draw[->] (0,0,-1.5) -- (0,0,-0.5);
    \end{scope}
  \end{tikzpicture}
  \caption{Dreidimensionale Anordnung: Zwei Spulen zwischen denen das Atom sitzt.}
  \label{fig:2014-04-09-4}
\end{figure}
