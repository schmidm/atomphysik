% Vorlesung vom 20.02.2014
% TeX: Michi

\chapter[Theoretische Behandlung der Laserkühlung]{Theoretische Behandlung der Laserkühlung anhand der Dopplerkühlung}

Eine wichtige Anwendung der spontanen Lichtkraft
%
\begin{align}
  \bm{F}_{\text{spont}} &= \hbar \bm{k}_L \frac{\Gamma}{2}\frac{S}{S+1}
\end{align}
%
ist die Laserkühlung (engl.: doppler cooling) von Atomen. Im Folgenden soll das Prinzip der \acct{Dopplerkühlung} näher betrachtet werden. Die Atome sind hierbei in einem zwei-Niveau-System mit einem Energieunterschied von $\hbar \omega_0$. Sie sind zwischen zwei Laser gleicher Frequenz $\omega$ lokalisiert die von beiden Seiten eingestrahlt werden. Die Frequenz soll nahe der Resonanzfrequenz der Atome sein, bei einer Verstimmung von $\delta = \omega - \omega_0 < 0$. Um die Diskussion zu vereinfachen betrachten wir den eindimensionalen Fall (insgesamt zwei Laser in $z$-Richtung ausgerichtet). Die Laserstrahlen haben hierbei die Wellenvektoren \[ \bm{k} \parallel \bm{z} \quad \text{und} \quad -\bm{k} \parallel -\bm{z}. \] Ein Atom im Feld beider Laser durchläuft Fluoreszenzzyklen. Ein Fluoreszenzzyklus besteht hierbei aus der Absorption eines Photons eines Lasers im Grundzustand. Das Atom geht somit in einen angeregten Zustand über. Spontane Emission führt zum Rückfall in den Grundzustand. 
%
\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}[gfx]
    \shade[ball color=white] (0,0) circle (0.3);
    \shade[ball color=white] (0.5,1) circle (0.3);
    \shade[ball color=white] (-1.2,0.7) circle (0.3);
    \shade[ball color=white] (-0.5,-1) circle (0.3);
    \draw[->,decorate,decoration={snake,post length=1mm}]  (5,0) -- (1,0);
    \draw[->,decorate,decoration={snake,post length=1mm}] (-6,0) -- (-2,0);
    \draw[->] (-5,1) -- node[above] {$\bm{k}$} (-3,1) ;
    \draw[<-] (2,1) -- node[above] {$-\bm{k}$} (4,1) ;
    \node at (-0.5,-1.7) {Atome};
    \node at (-4,-1.7) {Laserstrahl $(+)$};
    \node at (3,-1.7) {Laserstrahl $(-)$};
  \end{tikzpicture}
  \caption{Das Prinzip der Laserkühlung}
  \label{fig:2014-02-20_1}
\end{figure}   
%
Sei $n_+(v)$ die Anzahl der Fluoreszenzzyklen pro Sekunde die ein Atom mit der Geschwindigkeit $v$ mit der Absorption von Photonen mit Wellenvektor $\bm{k}$ durchläuft und $n_-(v)$ die Anzahl der Fluoreszenzzyklen mit Absorption von Photonen mit Wellenvektor $-\bm{k}$. Bewegt sich ein Atom nach links, vergleiche hierzu Abbildung~\ref{fig:2014-02-20_1}, wird es gemäß dem Dopplereffekt Photonen der Frequenz $\omega - k v$ vom Laser $\bm{k}$ und Photonen der Frequenz $\omega + kv$ vom Laser mit $-\bm{k}$ sehen. Aufgrund der negativen Verstimmung der beiden Laser sind die Photonen mit Wellenvektor $\bm{k}$ näher an der Resonanzfrequenz und werden im höheren Maße absorbiert als jene mit $-\bm{k}$. Dies führt zu einer Kraft in $+z$ Richtung. Umgekehrt für Atome die nach rechts gehen eine Kraft in $-z$ Richtung. Zusammenfassend werden also Atome in $+z$ Richtung bevorzugt Photonen mit Wellenvektor $\bm{k}$ absorbieren und Atome in $-z$ Richtung bevorzugt Photonen mit Wellenvektor $-\bm{k}$. In beiden Fällen werden die Atome abgebremst und eine Art viskose Kraft verspüren. Die durchschnittliche Kraft auf ein Atom mit der Geschwindigkeit $v$ ist demnach
%
\begin{align*}
  \Braket{\bm{F}} &= \hbar \bm{k} \left[n_+(v) - n_-(v)\right],
\end{align*}
% 
mit 
%
\begin{align}
  n_\pm(v) &= \frac{\Gamma}{4} \frac{\omega_R^2}{\left(\delta \mp kv\right)^2 + \Gamma^2/4}. \label{eq:2014-02-20-1}
\end{align}
%
Entwickeln von Gleichung~\eqref{eq:2014-02-20-1} in Potenzen der Geschwindigkeit mit Ordnung $v$ führt zu
%
\begin{align*}
  n_{\pm}(v) &= \frac{\Gamma \omega_R^4/4}{\delta^2 + \Gamma^2/4}\left( 1 \pm \frac{2 \delta k v}{\delta^2 + \Gamma^2/4} \right).
\end{align*}
%
Diese Gleichung gibt die durchschnittliche Anzahl von Fluoreszenzzyklen pro Sekunde $2 n_0$
%
\begin{align*}
  n_0 &= \frac{1}{2}\left[ n_+(v) + n_-(v) \right] = \frac{\Gamma \omega_R^2/4}{\delta^2 + \Gamma^2/4} = \frac{\Gamma}{2}S
\end{align*}
%
und eine Kraft proportional zu
%
\begin{align}
  n_+(v) - n_-(v) &= n_0 \frac{4 \delta k v}{\delta^2 + \Gamma^2 /4} \nonumber \\
  \implies \Braket{\bm{F}} & =\hbar \bm{k} \left[n_+(v) - n_-(v)\right] \nonumber \\
  &= n_0 v \frac{4 \hbar \delta k^2}{\delta^2 + \Gamma^2/4} \bm{k}. \label{eq:2014-02-20-2}
\end{align}
%
Die Viskosität $\gamma$ ist definiert als 
%
\begin{align*}
  \frac{\diff v}{\diff t} &= - \gamma v.
\end{align*}
%
Für Gleichung~\ref{eq:2014-02-20-2} bedeutet dies
%
\begin{align*}
  \gamma &= - \frac{\Braket{\bm{F}}}{M v} = - n_0 \frac{4 \hbar^2 k^2}{M} \frac{\delta}{\delta^2 + \Gamma^2/4}.
\end{align*}
%
$\gamma$ ist positiv, da $\delta<0$. Ist $n_0$ konstant, so ist die Viskosität maximal bei $\delta=-\Gamma/2$, d.h. 
%
\begin{align*}
  \gamma_{\text{max}} &= \frac{4 \hbar^2 k^2 }{M \Gamma} n_0 = \frac{8 n_0 \hbar^2 k^2}{\hbar \Gamma 2 M} = \frac{8 n_0}{\hbar \Gamma} E_R,
\end{align*}
%
wobei $E_R = Mv_R^2/2$ die Rückstoßenergie ist, d.h. die kinetische Energie wenn ein Atom ein Photon de Impuls $\hbar k$ emittiert und genauso die Energie die ein Atom annimmt bei Absorption eines Photons mit Impuls $\hbar k$. $v_R$ ist dementsprechend die Rückstoßgeschwindigkeit. Numerische Werte für $\gamma$ ergeben eine Beziehung der Form
%
\begin{align*}
  \gamma \approx 5 \cdot 10^{-3} n_0 = 2.5 \cdot 10^{-3} \Gamma S.
\end{align*}
%
Mit $S\ll1$ folgt
%
\begin{align*}
  \Gamma^{-1} \ll n_0^{-1} \ll \gamma^{-1}.
\end{align*}
%
Unter diesen Umständen erhalten wir drei Zeitskalen im Problem, siehe dazu Abbildung~\ref{fig:2014-02-20_2}.
%
\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (-0.1,0) node[left] {$g$} -- (7,0) node[right] {$t$};
    \draw[->] (0,-0.3) -- (0,4);
    \draw[dashed] (-0.1,3) node[left] {$e$} -- (7,3);
    \draw (0.5,0) rectangle (1.2,3);
    \draw[|<->|] (0.5,3.2) -- node[above] {$\Gamma^{-1}$} (1.2,3.2);
    \draw (2.5,0) rectangle (3.5,3);
    \draw (5,0) rectangle (5.8,3);
    \draw[<->] (3.5,1.5) -- node[above] {$n_0^{-1}$} (5,1.5);
  \end{tikzpicture}
  \caption{Eine Sequenz von Fluoreszenzzyklen}
  \label{fig:2014-02-20_2}
\end{figure}   
%
Die Relation $\Gamma^{-1} \ll n_0^{-1}$ zeigt, dass die Fluoreszenzzyklen nicht überlappen und unabhängig sind. Sei $\delta t$ ein Zeitintervall mit $\Gamma^{1} \ll \delta t \ll \gamma^{-1}$ und $N_{\pm}$ die Anzahl der Zyklen $\pm k$ in diesem Intervall. Da $\delta t \ll \gamma^{-1}$ hat die Geschwindigkeit der Atome nicht die Zeit sich merklich unter der Viskosenkraft im Intervall $\delta t$ zu ändern. Deshalb können wir über das Intervall mitteln 
%
\begin{align*}
  \Braket{N_{\pm}} = n_{\pm}(v) \delta t.
\end{align*}
%
Sei $\mathrm{p}(N_+,N_-,\delta t)$ die Wahrscheinlichkeit $N_+(+k)$ Zyklen und $N_-(-k)$ Zyklen im Intervall $\delta t$ zu beobachten. Da die Fluoreszenzzyklen unabhängig sind, folgt die Wahrscheinlichkeit der Poissonverteilung (Wahrscheinlichkeitsverteilung)
%
\begin{align*}
  \mathrm{p}(N_+,N_-;\delta t) &= \frac{\Braket{N_+}^{N_+} \Braket{N_-}^{N_-} \ee^{-\left(\Braket{N_+} - \Braket{N_-}\right)}}{N_+!N_-!}.
\end{align*}
% 
Wir verwenden $\hbar q, \ldots \hbar q_{N_+ + N_-}$ um $N_+ + N_-$ Impulse der spontan emittierten Photonen eines Atoms im Intervall $\delta t$ zu bezeichnen. Dementsprechend bezeichnen wir mit $ \hbar Y$ 
%
\begin{align*}
  \hbar Y &= \hbar q_1 + \ldots + \hbar q_{N_+ + N_-}.
\end{align*}
%
Die emittierten Photonen sind nicht miteinander korreliert und $\braket{Y} = 0$. Die durchschnittliche Variation der Impulse während der Zeit $\delta t$ is aufgrund der absorbierten Photonen
%
\begin{align*}
  \Braket{p(\delta t)} &= \left(n_+(v) - n_-(v)\right)\hbar k \delta t.
\end{align*}
%
Die Varianz ist folglich:
%
\begin{align}
  \Delta p^2(\delta t) &= \Braket{p^2(\delta t) - \braket{p(\delta t)}^2}. \label{eq:2014-02-20-3}
\end{align}
%
Da die spontanen und absorbierten Photonen nicht korreliert sind und $\Braket{\hbar Y} = 0$ können die beiden Beiträge separat betrachtet werden:
%
\begin{enumerate}
  \item Der Beitrag der Varianz der absorbierten Photonen ist
    %
    \begin{align*}
      \left.\Delta p^2(\delta t)\right|_{\text{abs}} &= \hbar^2 k^2 \Braket{(N_+ - N_-)^2 - \left(\Braket{N_-} - \Braket{N_+}\right)^2} = 2 \hbar^2 k^2 n_0 \delta t
    \end{align*}
    %
    wobei aus der klassischen Eigenschaft der Poissonverteilung $\Delta N_{\pm}^2 = \braket{N_\pm}$ sowie der Tatsache, dass die $+$ und $-$ Zyklen unabhängig sind,
    %
    \begin{align*}
      \Braket{N_+N_-} &= \Braket{N_+}\Braket{N_-}.
    \end{align*}
    %
    folgt.
  \item Der Beitrag der emittierten Photonen ist
    %
    \begin{align*}
      \Delta p^2(\delta t)\bigg|_{\text{em}} &= \hbar^2 \Braket{Y} = \hbar \sum\limits_{i=1}^{N_++N_-} q_i^2 = \hbar^2 k^2 \Braket{N} = 2 n_0 \hbar^2 k^2 \delta t.
    \end{align*}
    %
\end{enumerate}
%
Da wir uns auf den eindimensionalen Fall beschränken gilt für die Photonenimpulse $\pm \hbar k$ eine Wahrscheinlichkeit von $1/2$. Somit finden wir für Gleichung~\ref{eq:2014-02-20-3}
%
\begin{align*}
  \Delta p^2(\delta t) &= 4 n_0 \hbar^2 k^2 \delta t.
\end{align*}
%
Diese Resultat stellt ein Random-Walk im eindimensionalen Impulsraum dar. In solch einem Random-Walk wird ein Schritt der Länge $\ell$ von rechts nach links mit der Wahrscheinlichkeit $1/2$ geführt. Nach $N$ Schritten gilt also für die durchschnittliche Distanz $\Braket{x} = 0$. Die quadratische Distanz ist aber nicht null sondern
%
\begin{align*}
  \Braket{x^2} &= \Delta x^2 = N \ell^2.
\end{align*}
% 
Braucht jeder Schritt die Zeit $\tau$, so ergibt sich nach einer Zeit $\delta t = N \tau$ die quadratische Abweichung
%
\begin{align*}
  \Delta x^2 &= \frac{\ell^2}{\tau} \delta t = 2 D \delta t,
\end{align*}
%
wobei $D$ der Diffusionskoeffizient ist. Die kinetische Energie eines Atoms nimmt hierbei um \[ \frac{\Delta p^2 (\delta t)}{2 M} \] zu. Daher neigt die Diffusion dazu die kinetische Energie zu steigern. Nimmt man Bezug auf die statistische Mechanik gilt
%
\begin{align*}
  E &= \frac{1}{2} k_\mathrm{B} T,
\end{align*}
%
wobei $T$ eine fiktive Temperatur darstellen soll. Wir sehen also, dass die Atome durch die spontane Emission erhitzt werden, d.h. dass $E$ zunimmt, was einer thermischen Bewegung entspricht. Da jedoch kein thermodynamisches Gleichgewicht vorliegt, ist $T$ nur fiktiv, d.h. $T$ ist für ein isoliertes Atom wohl definiert. Die Viskosität dagegen neigt dazu die Atome zu bremsen, also sie zu kühlen. Sind beide Effekte im Gleichgewicht erhalten wir eine Gleichgewichtstemperatur, eine fiktive Temperatur der Atome im stationäre Fall. Diese Temperatur bietet einen Weg die durchschnittliche Geschwindigkeit zu messen. Nach der Definition der Viskosität erhalten wir für die zeitliche Änderung der Energie
%
\begin{align*}
  \frac{\diff E}{\diff t}\bigg|_{\text{vis}} &= \frac{1}{2} M \frac{\diff}{\diff t} v^2 = - M \gamma v^2 = -\frac{\gamma p^2}{M}.
\end{align*}
% 
Mit dem Effekt der spontanen Emission finden wir
%
\begin{align*}
  \frac{\diff E}{\diff t} &= \frac{2 n_0 \hbar^2 k^2}{M} - \frac{\gamma p^2}{M}.
\end{align*}
%
Im stationären Fall gilt $\diff E/\diff t = 0$ und wir erhalten den Gleichgewichtswert
%
\begin{align*}
  p_{eq}^2 &= \frac{2 n_0 \hbar^2 k^2}{\gamma_{\text{max}}} = \frac{1}{2} \hbar \Gamma M,
\end{align*}
%
wobei wir $\gamma = \gamma_{\text{max}}$ gewählt haben. Damit erhalten wir die Temperatur 
%
\begin{align}
  \boxed{ k_\mathrm{B} T_D = \frac{p_{eq}^2}{M} = \frac{1}{2} \hbar \Gamma }
\end{align}
% 
Die Temperatur $T_D$ wir auch als \acct{Dopplertemperatur} bezeichnet. Eine analoge Schreibweise des stationären Falls ist
%
\begin{align*}
  D = \gamma p_{eq}^2 = M \gamma k_\mathrm{B} T.
\end{align*}
% 
Diese Relation ist auch als \acct{Einstein-Relation} bekannt und verbindet dem Diffusionskoeffizienten mit der Viskosität. Es zeigt sich also, dass der Ursprung des dissipativen Prozesses die spontanen Emission ist, die mit der nicht einheitlichen Evolution korrespondiert. 
