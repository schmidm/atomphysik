% Vorlesung am 08.01.2014
% TeX: Michi

\renewcommand{\printfile}{2014-01-08}

\chapter{Atom-Licht Wechselwirkung}

\section{Zwei Niveau System: Rabi Oszillation}

\subsection{Jaynes-Cummings-Modell}

Das \acct{Jaynes-Cummings-Modell} stellt eine vollständige quantenmechanische Beschreibung der Atom-Licht Wechselwirkung dar. Betrachten wir zum Beispiel die verschiedenen Zustände eines Atoms wie in Abbildung~\ref{fig:2014-01-08_1}

\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}[gfx]
    \draw (0,0) node (A) {} -- (0.5,0) node (B) {};
    \draw (0,1) -- (0.5,1);
    \draw (0,1.5) -- (0.5,1.5);
    \draw (-0.5,1.5) node (C) {} -- (-1,1.5) node (D) {};
    \draw (1,0) -- (1.5,0);
    \draw (1,1) -- (1.5,1);
    \draw (1,1.5) -- (1.5,1.5);
    \draw (2,1.5) -- (2.5,1.5);
    \node[left] at (-1.25,0) {$\ket{g}$};
    \node[left] at (-1.25,1.5) {$\ket{e}$};
    \node[right] at (2.5,1.5) {$m$ $P_{3/2}$};
    \node[right] at (2.5,1.0) {$m$ $P_{1/2}$};
    \node[right] at (2.5,0.0) {$m$ $S_{1/2}$};
    \node[below] at (0.75,-0.25) {Magn.\ Unterzustände ($m_j$)};
    \draw[MidnightBlue,<->] (0.25,0) -- (-0.75,1.5);
    \node[draw,DarkOrange3,rotate fit=34,rounded corners,fit=(A) (B) (C) (D)] {};
    \draw[|<->|] (4,1.5) -- node[right] {Feinstruktur} (4,1.0);
  \end{tikzpicture}
  \caption{Atomniveaus, wobei $\Ket{e}$ ein angeregte Zustand sein soll und $\Ket{g}$ der Grundzustand}
  \label{fig:2014-01-08_1}
\end{figure}

Durch Auswahlregeln, wie etwa der Polarisation des Lichts und durch Frequenzschärfe, bspw. durch ein Laser kann das Problem (oft) auf zwei Niveaus reduziert werden, wie in Abbildung~\ref{fig:2014-01-08_2} zu sehen ist.

\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}[gfx]
    \draw (0,2) -- (1,2) node[right] {$\ket{e}$};
    \draw[<->] (0.5,2) -- node[right] {$\hbar \omega_0$} (0.5,0);
    \draw (0,0) -- (1,0) node[right] {$\ket{g}$};
  \end{tikzpicture}
  \caption{Zwei Niveau System}
  \label{fig:2014-01-08_2}
\end{figure}

\begin{theorem}[Definition:]
  Wir definieren folgenden Operator
  %
  \begin{align}
    \hat{\sigma}_{ij} \coloneq \ket{i}\bra{j} \qquad i,j = e,g
  \end{align}
  %
  D.h. wir können einen Erzeuger $\hat{\sigma}_{eg}$ und Vernichter $\hat{\sigma}_{ge}$ der atomaren Anregung finden der Form
  %
  \begin{align}
    \hat{\sigma}_{eg} &= \ket{e}\bra{g} \label{eq: 2014-01-08_1} \\
    \hat{\sigma}_{ge} &= \ket{g}\bra{e} \label{eq: 2014-01-08_2}
  \end{align}
  %
\end{theorem}

\begin{example}
  Wirkung von \eqref{eq: 2014-01-08_1} und \eqref{eq: 2014-01-08_2} auf den Grund-, bzw. angeregten Zustand
  %
  \begin{align*}
    \hat{\sigma}_{eg} \ket{g} &= \ket{e}\braket{g|g} = \ket{e} \\
    \hat{\sigma}_{ge} \ket{e} &= \ket{g}\braket{e|e} = \ket{g} \\
    \hat{\sigma}_{ge} \ket{g} &= \ket{g}\braket{e|g} = 0 
  \end{align*}
  %
\end{example}

Hintereinanderausführung von \eqref{eq: 2014-01-08_1} und \eqref{eq: 2014-01-08_2} ergibt
%
\begin{align*}
  \hat{\sigma}_{eg}\hat{\sigma}_{ge} &= \ket{e}\braket{g|g}\bra{e} \\
  &= \ket{e}\bra{e} = \hat{\sigma}_{ee} \\
  \braket{\hat{\sigma}_{eg}\hat{\sigma}_{ge}} &= \braket{\psi|e}\braket{e|\psi}\\
  &= \left|\braket{e|\psi}\right|^2 = P_{e}
\end{align*}
%
$P_e$ ist hierbei die Wahrscheinlichkeit, dass das Atom im angeregten Zustand ist, $P_g$ wäre dementsprechend die Wahrscheinlichkeit, dass das Atom im Grundzustand ist. 

Der \acct*{Hamiltonoperator des Zwei Niveau System} \index{Hamiltonoperator ! Zwei Niveau System} lautet dementsprechend
%
\begin{align}
  \boxed{ \hat{H}_{\text{Atom}} = \hbar \omega_0 \hat{\sigma}_{eg}\hat{\sigma}_{ge}} \label{eq: 2014-01-08_3}
\end{align}
%

\minisec{Quantisiertes Lichtfeld}
Im Folgenden betrachten wir ein Quantisierungsvolumen $V$.
\[
  V = L^3.
\]

Das elektrische Feld $\bm{E}(\bm{r},t)$ in der Box kann zerlegt werden in eine Summe aus ebenen Wellen (Fourier Transformation).
%
\begin{align*}
  \bm{E}(\bm{r},t) &= \sum\limits_{\bm{k},p} \left[ \bm{e}_p E_{\bm{k}} \varepsilon_k \exp\left(-\ii \omega_k t - \ii \bm{k}\cdot \bm{r}\right) + \text{c.c.} \right]
\end{align*}
% 
Hierbei gilt für den Wellenvektor
%
\begin{align*}
  \bm{k} &= \begin{pmatrix} k_x \\ k_y \\ k_z\end{pmatrix}\quad,\quad k_i = \frac{2 \pi m_i}{L}
\end{align*}
%
$E_\bm{k}$ entspricht der Amplitude der Mode und $\bm{e}_p$ dem Polarisationsvektor (zu jedem $\bm{k}$ gehören 2 orthogonale Polarisationen). Der Faktor $\varepsilon_k$ ist nur ein Vorfaktor und ist
\[
  \varepsilon_k = \sqrt{\frac{\hbar \omega_k}{2 \varepsilon_0 V}}.
\]

\begin{notice}
  Die Polarisation erfolgt in $p=x,y$ oder in $p=+,-$, letzteres entspricht einer sphärischen Basis.
\end{notice}

Ein endliches Quantisierungsvolumen $V$ hat diskrete Moden zur Folge. Für $V \to \infty$ erhalten wir also ein kontinuierliches Spektrum, d.h. die Summe geht in ein Integral über und zwar der Form
\[
  \sum\limits_{k}\ldots \to \int \ldots \diff ^3k.
\]

\begin{theorem}[Planck:] 
  Jede einzelne Mode kann nur diskrete Energien annehmen!
\end{theorem}

Die klassische Amplitude $E_k$ geht also in quantenmechanische Operatoren über.
\[
  E_k,E_k^\ast \to \hat{\tilde{a}}_k, \hat{\tilde{a}}_k^\dagger. 
\] 
Für die vollständige Herleitung sei auf die Literatur verwiesen. Das Ergebnis ergibt sich somit zu
%
\begin{align}
  \hat{\bm{E}}(\bm{r},t) = \sum\limits_{\bm{k},p} \bm{e}_p \varepsilon_k \left[ \hat{\tilde{a}}_{k,p} \exp\left(-\ii \omega_k t+ \ii \bm{k}\cdot \bm{r} \right) + \hat{\tilde{a}}_{k,p}^\dagger \exp\left(\ii \omega_k t-+ \ii \bm{k}\cdot \bm{r} \right) \right]
\end{align}
%
Jede einzelne Mode verhält sich wie ein quantenmechanischer harmonischer Oszillator, d.h. für eine Mode $\bm{k}, p$ gilt:
\[
  E_{\bm{k},p} = \hbar \omega_k \left(n + \frac{1}{2}\right)
\]
Das Energiespektrum, bzw. die Zustände sind in Abbildung~\ref{fig:2014-01-08_4} dargestellt.

\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}[gfx,yscale=0.7]
    \draw[dashed] (0,0) node[left] {$0$} -- (0.5,0);
    \foreach \n in {0,1,2} {
      \draw (0,\n+1/2) -- (0.5,\n+1/2) node[right] {$\ket{\n}$};
    }
    \node at (0.25,3.25) {$\vdots$};
    \draw (0,4) -- (0.5,4) node[right] {$\ket{n}$};
  \end{tikzpicture}
  \caption{Energien des HO zu verschiedenen Zuständen $\ket{n}$ für eine bestimmte Mode $\bm{k}$.}
  \label{fig:2014-01-08_4}
\end{figure}

$\ket{n}$ entspricht dem Zustand mit $n$ Photonen in der Mode. Dieser wird auch als \acct{Fock Zustand} (engl. number state) bezeichnet. 

$\ket{0}$ entspricht dem \frqq Vakuumzustand\flqq\ ($0$ Photonen), wobei $E_0 =  \hbar \omega_0/2 > 0$, die Nullpunktenergie ist. Sie ist eine Konsequenz der Heisenbergschen Unschärferelation (vgl. harmonischer Oszillator). 

Im folgenden wollen wir nun Erzeuger und Vernichter einführen (Übergang ins Heisenbergbild, bzw. Wechselwirkungsbild):
%
\begin{align}
  \hat{a}_{\bm{k},p}^\dagger &= \mathrm{e^{-\ii \omega t }} \hat{\tilde{a}}_{\bm{k},p}^{\dagger} \\
  \hat{a}_{\bm{k},p} &= \mathrm{e^{\ii \omega t }} \hat{\tilde{a}}_{\bm{k},p}
\end{align}
%
\begin{notice}
  Der Übergang ins Heisenbergbild stellt sich hier als sinnvoll heraus, da dann die Zeitabhängigkeit eliminiert werden kann.
\end{notice}

$\hat{a}_{\bm{k},p}^\dagger$, bzw. $\hat{a}_{\bm{k},p}$ sind Erzeuger, bzw. Vernichter eines Photons in den Moden $\bm{k},p$. Wir betrachten genau eine Mode mit Frequenz \[\omega_L = 2 \pi f_L \] und definierter Polarisation. Die Wirkung der Vernichter und Erzeuger ist:
%
\begin{align*}
  \hat{a}^\dagger \ket{n} &= \sqrt{n+1} \ket{n+1} \\
  \hat{a} \ket{n+1} &= \sqrt{n+1} \ket{n}.
\end{align*}
%
Der \acct*{Hamiltonoperator des \frqq single-mode\flqq\ Feldes} \index{Hamiltonoperator ! single-mode Feld} ist dementsprechend
%
\begin{align}
  \boxed{ \hat{H}_{\text{Field}} = \hbar \omega_L \left(\hat{a}^\dagger \hat{a} + \frac{1}{2}\right) }
\end{align}
%
Durch verschieben des Nullpunkts, kann jedoch der hintere Term vernachlässigt werden. Hierbei ist \[n \coloneq \hat{a}^\dagger \hat{a}\] der \acct{Anzahloperator}.


\minisec{Zustand des Gesamtsystems}

Für die Wellenfunktion gilt
%
\begin{align*}
  \ket{\psi} &= \ket{\text{Atom}} \otimes \ket{\text{Feld}} = \ket{\text{Atom},\text{Feld}}.
\end{align*}
%
\begin{example}
  \frqq bare states\flqq:
  %
  \begin{align*}
    \ket{e,n} \quad,\quad \ket{g,n+1}
  \end{align*}
  %
\end{example}

Ein Übergang von $\Ket{e} \to \Ket{g}$ bei festgehaltener Photonenzahl $n$ steht im Folgenden mit der Frequenz $\omega_0$ in Verbindung. Ist diese Frequenz etwa so groß wie die Frequenz des Übergangs 
%
\begin{align}
  \Ket{i,n} \rightarrow \ket{i,n \pm 1}  \qquad i=e,g \quad ,
  \label{Marcel eingefuegt 01.08}
\end{align}
%
also
 \[\omega_L \approx \omega_0\] 
so hat dies zur Folge, dass \[\Delta = \omega_0 - \omega_L \ll \omega_0 \] gilt. Dabei ist $\Delta$ diejenige Frequenz, die den Übergang in das nächstgelegene Energieniveau beschreibt. (siehe Abbildung~\ref{fig:2014-01-08_5}). 

\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (0,0.5) -- (0,4) node[left] {$E/\hbar$};
    \draw[MidnightBlue] (0.5,0.8) coordinate (g 1) -- (1.5,0.8) node[right] {$|g,n-1\rangle$};
    \draw[MidnightBlue] (0.5,1.8) coordinate (g 2) -- (1.5,1.8) node[right] {$|g,n\rangle$};
    \draw[MidnightBlue] (0.5,2.8) coordinate (g 3) -- (1.5,2.8) node[right] {$|g,n+1\rangle$};
    \draw[DarkOrange3] (0.5,1.2) coordinate (e 1) -- (1.5,1.2) node[right] {$|e,n-2\rangle$};
    \draw[DarkOrange3] (0.5,2.2) coordinate (e 2) -- (1.5,2.2) node[right] {$|e,n-1\rangle$};
    \draw[DarkOrange3] (0.5,3.2) coordinate (e 3) -- (1.5,3.2) node[right] {$|e,n\rangle$};
    \draw[decorate,decoration=brace] (-0.1,1.8) -- node[left] {$\Delta$} (-0.1,2.2);
    \draw[<->] (g 2) -- node[right] {$\omega_0$} (e 3);
    \draw[<->] (g 1) -- node[left] {$\omega_L$} (g 2);
  \end{tikzpicture}
  \caption{\frqq bare states\flqq\ Leiter}
  \label{fig:2014-01-08_5}
\end{figure}

\minisec{Wechselwirkung}

In Erinnerung an die elektrischen Dipolnäherung wissen wir, dass die Wellenlänge des Atoms viel kleiner ist, als die Wellenlänge des treibenden Feldes. D.h. wir entwickeln $\exp\left(\ii \bm{k} \cdot \bm{r}\right)$ um $\bm{r} \cdot \bm{k} = 0$, da $r \approx \SI{e-10}{\m}$ und $k \approx \SI{e7}{\per\m}$ und somit \[ \bm{r} \cdot \bm{k} \approx \num{e-3} \]
%
%
\begin{align*}
  \exp\left(\ii \bm{k} \cdot \bm{r}\right) &= 1 + \ii \bm{k} \cdot \bm{r} + \ldots \\
  &\approx 1 
\end{align*}
%
Der Hamiltonoperator der Wechselwirkung ist dementsprechend (wobei $\hat{\bm{d}} = e \hat{\bm{r}}$)
%
\begin{align}
  \hat{H}_{\mathrm{WW}} &= - \hat{\bm{d}} \cdot \bm{E} \\
  &= e \sum\limits_{i,j} \ket{i}\braket{i|\bm{r}|j}\bra{j}
\end{align}
%
Für ein zwei-Niveau-System, gilt $i,j=e,g$. Wobei
%
\begin{itemize}
  \item $\bm{\mu}_{i,j}= e \braket{i|\bm{r}|j} $ das Dipolmatrixelement ist
  \item $\bm{\mu}_{e,e} = \bm{\mu}_{g,g} = 0$ 
  \item $\bm{\mu}_{e,g} = \bm{\mu}_{g,e}$
\end{itemize}
%
Damit vereinfacht sich die Summe zu
%
\begin{align}
  \hat{H}_{\mathrm{WW}} &=  \bm{\mu}_{eg} \left(\hat{\sigma}_{eg} + \hat{\sigma}_{ge}\right)
\end{align}
%
Wird der Operator für $\bm{E}$ (single mode) benutzt gilt
\[
  \hat{\bm{E}} = \bm{e}_p \varepsilon_k \left(\hat{a} + \hat{a}^\dagger\right)
\]
Der Hamiltonoperator der Wechselwirkung kann dann umgeschrieben werden.
%
\begin{align*}
  \hat{H}_{\mathrm{WW}} &= - \hat{\bm{d}}\cdot \hat{\bm{E}} \\
  &= \hbar \Omega \left(\hat{\sigma}_{eg} + \hat{\sigma}_{ge}\right)\left(\hat{a} + \hat{a}^\dagger\right)
\end{align*}
%
Hierbei ist \[ \Omega = \frac{\bm{\mu}_{eg} \cdot \bm{e}_p \cdot \varepsilon_k}{\hbar} \] die \acct{Rabi Frequenz}. Sie gibt im wesentlichen die Stärke der Kopplung an. Der Hamiltonoperator der Wechselwirkung hat nach expliziten ausmultiplizieren vier Terme, zwei \frqq energieerhaltenede\flqq\ Terme und zwei >>nicht energieerhaltende\flqq Terme. Die energieerhaltenden Terme sind:
%
\begin{align*}
  \hat{\sigma}_{eg} \cdot \hat{a} &: \quad \text{Erzeugung der atomaren Anregung und Vernichtung eines Photons} \\
  \hat{\sigma}_{ge} \cdot \hat{a}^\dagger &: \quad \text{Vernichtung der atomaren Anregung und Erzeugung eines Photons}
\end{align*}
%
Sie koppeln eine zusammenliegendes Paar von \frqq bare states\flqq. Die >>nicht ernergieerhaltenden\flqq\ Terme sind dementsprechend:
%
\begin{align*}
  \hat{\sigma}_{ge} \cdot \hat{a} &: \quad \text{Verneichtung der atomaren Anregung und Vernichtung eines Photons} \\
  \hat{\sigma}_{eg} \cdot \hat{a}^\dagger &: \quad \text{Erzeugung der atomaren Anregung und Erzeugung eines Photons}
\end{align*}
%
Sie stellen eine Kopplung zwischen den übernächsten Paaren dar. 

\begin{example}
  \begin{align}
    \hat{\sigma}_{eg} \cdot \hat{a} \ket{e,n} &\to \ket{e,n-1} \\
    \hat{\sigma}_{ge} \cdot \hat{a}^\dagger \ket{e,n-1} &\to \ket{g,n} \\
    \hat{\sigma}_{ge} \cdot \hat{a} \ket{e,n} &\to \ket{g,n-1} \\
    \hat{\sigma}_{eg} \cdot \hat{a}^\dagger \ket{g,n} &\to \ket{e,n+1}
  \end{align}
\end{example} 

Für $\Delta \ll \omega_0$ können die Beiträge der \frqq nicht energieerhaltenden\flqq\ Terme vernachlässigt werden (auch >>\acct{Rotationg Wave Approximation}\flqq genannt). 

\minisec{Vollständiger Hamiltonoperator}

Der vollständige Hamiltonoperator setzte sich dementsprechend aus $\hat{H}_{\text{Atom}}$, $\hat{H}_{\text{Field}}$ und $\hat{H}_{\text{WW}}$ zusammen:
%
\begin{align}
  \boxed{\hat{H}_{\text{ges}} = \hbar \omega_L \hat{a}^\dagger \hat{a} + \hbar \omega_0 \hat{\sigma}_{ge} \hat{\sigma}_{eg} - \hbar \Omega \left(\hat{\sigma}_{eg} \hat{a} + \hat{\sigma}_{ge} \hat{a}^\dagger\right)} \label{eq: 2014-01-08_4}
\end{align}
%
Der Hamiltonoperator \eqref{eq: 2014-01-08_4} in Matrixdarstellung hat eine Blockstruktur bestehend aus $2\times2$ Matrizen
%
\begin{align}
  \hat{H}_{\text{ges}} &= \begin{pmatrix}
    (2 \times 2) &              &           & 0 \\
                 & (2 \times 2) &           &   \\
                 &              & \ddots    &   \\
    0            &              &           & (2 \times 2) \\
  \end{pmatrix}
\end{align}
%
Dieser Hamiltonoperator wird auch als \acct*{Jaynes-Cummings Hamiltonoperator} \index{Hamiltonoperator ! Jaynes-Cummings} bezeichnet und ist exakt lösbar. Die Eigenzustände werden auch als \frqq dressed states\flqq\ bezeichnet. Zu jedem $n$ existieren 2 Eigenzustände.
%
\begin{align*}
  \ket{\pm,n} &= \frac{1}{\sqrt{2}} \left(C_g^\pm \ket{g,n+1} \pm C_e^\pm \ket{e,n}\right)
\end{align*}
%
Die Variablen $C_g^\pm$ und $C_e^\pm$ können mittels Diagonalisierung von \eqref{eq: 2014-01-08_4} bestimmt werden.

\begin{notice}
  \begin{enumerate}
    \item Experimentelle Umsetzung in \frqq High-Finess-Resonatoren\flqq\ (z.B.: optisch und Grundzustand, Mikrowelle und Rydbergatom) 
    \item Auch ohne Photonen ($n=0$) gibt es eine Kopplung. Deren Stärke hängt vom Vorfaktor $\varepsilon_k$ ab. Durch \frqq Überhöhung\flqq\ einer Mode im Resonator kann diese Kopplung verstärkt werden (Vakuum Rabi Splitting)
    \item Ein Laserfeld (kohärenter Zustand) ist eine Superposition von Fock-Zuständen\[\ket{\alpha} = \ee^{-\frac{1}{2} | \alpha |^2} \sum\limits_{n=0}^{\infty} \frac{\alpha^n}{\sqrt{n!}} \ket{n} \]
    Für übliche Laserintensitäten
    %
    \begin{align*}
      \braket{\hat{n}} &= \braket{\alpha | \hat{n} | \alpha} \to \infty  \implies n+1 \approx n \\
      \frac{\sqrt{\sigma_n^2}}{\braket{\hat{n}}} &\to \frac{1}{\sqrt{n}} \to 0
    \end{align*}
    % 
    D.h. die Quantisierung kann oft vernachlässigt werden.
   \end{enumerate}
\end{notice}
