% Vorlesung am 25.06.2014
% TeX: Michi

\renewcommand{\printfile}{2014-06-25}

Die \acct{Gross-Pitaevskii Gleichung}, bzw. die nichtlineare Schrödingergleichung hat die Form
%
\begin{align}
  \ii \hbar \dot{\psi}(\bm{r},t) &= \left[-\frac{\hbar^2}{2m} \nabla^2 + V(\bm{r}) + g \vert \psi(\bm{r},t) \vert^2\right] \psi(\bm{r},t), \label{eq:2014-06-25-1}
\end{align}
%
wobei $g = \frac{4 \pi \hbar^2 a}{m}$ und $a$ die Streulänge ist. Unter der \acct{Thomas-Fermi Näherung} versteht man hierbei die Annahme, dass der Wechselwirkungsterm in Gleichung~\eqref{eq:2014-06-25-1} größer ist als der Anteil der kinetischen Energie, d.h. wir können diesen vernachlässigen. Die Lösung ist in diesem Fall
%
\begin{align*}
  n(\bm{r}) &= \frac{1}{g} \left(\mu - V(\bm{r})\right).
\end{align*}
%
Betrachten wir nun den Fall $a<0$, d.h. bei attraktiver Wechselwirkung. In der Falle muss hierbei die Anzahl der Atome unter einer kritischen Zahl $N_{\mathrm{crit}}$ liegen. Hierzu betrachten wir das \acct{Gross-Pitaevskii Energiefunktional} 
%
\begin{align}
  E[\phi] &= \int \left[ \frac{\hbar^2}{2m} \vert \nabla \phi \vert^2 + V_{\mathrm{trap}} \vert \phi \vert^2 + \frac{g}{2} \vert \phi \vert^4 \right] \diff \bm{r}. \label{eq:2014-06-25-2}
\end{align}
% 
Ein einfaches Modell bietet ein Gauß-Ansatz der Form
%
\begin{align*}
  \phi(\bm{r}) = \left(\frac{N}{w^3 a_{\mathrm{Ho}}^3 \pi{3/2}}\right)^{1/2} \ee^{-\frac{r^2}{2 w^2 a_{\mathrm{H0}}^2}},
\end{align*}
%
wobei $w$ die breite des Gauß ist. Vergleichen wir die mittels des Ansatzes gefundene Energie mit der Energie des ungestörten harmonischen Oszillator, so erhalten wir
%
\begin{align*}
  \frac{E(w)}{N \hbar a_{\mathrm{H0}}} &= \frac{3}{4} \left(w^{-2} + w^2\right) - (2 \pi )^{-1/2} \frac{N \vert a \vert }{a_{\mathrm{H0}}} a^{-3}.
\end{align*}
%
\begin{figure}[htp]
\centering
\begin{tikzpicture}[gfx]
  \draw[->] (-.3,0) -- (3,0) node[below] {$w$};
  \draw[->] (0,-.3) -- (0,3) node[left] {$E/N$};
  \draw[MidnightBlue] (0,0) to[in=-170] (1.5,1) to[out=10,in=-100] (3,3);
  \draw[DarkOrange3] (0,0) to[in=180,looseness=.3] (.5,2) to[out=0,in=-170,looseness=.5] (1.5,1) to[out=10,in=-100] (3,3);
  \draw[Purple] (0,0) to[in=180,looseness=.3] (.5,3) to[out=0,in=-170,looseness=.5] (1.5,1) to[out=10,in=-100] (3,3);
\end{tikzpicture}
\caption{Energie $E(w)$ durch die Energie des ungestörten harmonischen Oszillator über der Gaußbreite $w$ aufgetragen.}
\label{fig:2014-06-25-1}
\end{figure}
%
In Abbildung~\ref{fig:2014-06-25-1} sehen wir das Verhalten dieser Funktion. Eine explizite Auswertung führt zu 
%
\begin{align*}
  \boxed{ \frac{N_{\mathrm{crit}} \vert a \vert}{a_{\mathrm{HO}}} = 0.57 }
\end{align*}
%
D.h. mittels dieser Gleichung ist es möglich die kritische Anzahl von Atomen zu bestimmen, bei der eine Bose-Einstein Kondensation möglich ist, falls $a<0$.

\begin{notice}
  Bei der attraktive Wechselwirkung stehen somit der Quantendruck und die Wechselwirkungsenergie in Konkurrenz.   
\end{notice}

Wie wir bereits in Abbildung~\ref{fig:2014-06-18-9} gesehen haben  stellt die Thomas-Fermi Näherung an den Rändern keine gute Lösung der Wellenfunktion dar (Sprünge und Unstetigkeiten). Die \acct{Healinglänge} $\xi$ eines Bose-Einstein-Kondensats stellt hierbei eine Korrektur zu Thomas-Fermi Näherung dar, es gilt
%
\begin{align*}
  E_{\mathrm{kin}} = E_{\mathrm{WW}} \implies \xi^2 = \frac{1}{8 \pi n a}.
\end{align*}
%

\minisec{Hierarchie der Längenskalen}

Im folgenden Abschnitt sei $a$ die Streulänge, $a_{\mathrm{HO}}$ die harmonische Oszillatorlänge, $n^{-1/3}$ der mittlere Teilchenabstand und $\xi$ die Healinglänge. Um eine gewünschte Hierarchie dieser Größen zu erhalten betrachten wir wieder das GP-Energiefunktional~\eqref{eq:2014-06-25-2} und vergleichen die kinetische und potenzielle Energie
%
\begin{align*}
  \frac{\hbar^2}{2m} \frac{1}{\text{Länge}^2} &= \frac{1}{2} m \omega^2 \, \text{Länge}^2 \\
  \implies \text{Länge}^4 &= \frac{\hbar^2}{m^2 \omega^2}.
\end{align*}
%
Vergleichen wir zudem die kinetische Energie mit der Wechselwirkungsenergie
%
\begin{align*}
  \frac{\hbar^2}{2m} \frac{1}{\text{Länge}} &= \frac{4 \pi a \hbar^2}{m} n \\
  \implies \text{Länge} &=  \frac{1}{\sqrt{8 \pi a n}} = \xi.
\end{align*}
%
Die Gross-Pitaevskii Gleichung ist nur für eine schwache Wechselwirkung $na^3 \ll 1$ gültig
%
\begin{align*}
  \frac{\xi^2}{n^{2/3}} = \frac{1}{8 \pi n^{1/3} a} \gg 1.
\end{align*}
%
Daraus folgt die gewünschte Hierarchie
%
\begin{align*}
  \boxed{ \xi \gg n^{-1/3} \gg a }
\end{align*}
%
Falls
%
\begin{align*}
  \frac{N a}{a_{\mathrm{HO}}} \gg 1 \implies \text{TF Regime} \implies R_{\mathrm{TF}} \gg a_{\mathrm{HO}}.
\end{align*}
%
Damit erhalten wir insgesamt
%
\begin{align}
  \boxed{ R_{\mathrm{TF}} > a_{\mathrm{HO}} > \xi > n^{-1/3} > a}
\end{align}
%
\begin{notice}
  Beispiel: In herkömmliche Fallen ($N=10^5$, $\omega_{x/y}=\SI{200}{\hertz}$, $\omega_z = \SI{20}{\hertz}$) misst man
  %
  \begin{align*}
    R_{\mathrm{TF}}=\SI{3}{\micro\metre} > a_{\mathrm{HO}}=\SI{760}{\nano\metre} > \xi = \SI{190}{\nano\metre} > n^{-1/3} = \SI{170}{\nano\metre} > a = \SI{5.5}{\nano\metre}
  \end{align*}
  %
\end{notice}

\section{Hydrodynamische Gleichungen}

Multiplizieren wir die Gross-Pitaevskii Gleichung~\eqref{eq:2014-06-25-1} mit $\psi^\ast$ von rechts und subtrahieren die mit komplexkonjugierte GP-Gleichung multipliziert mit $\psi$, also
\[
  \mathrm{GP} \psi^\ast - \mathrm{GP}^\ast \psi,
\]
so erhalten wir 
%
\begin{align*}
  \partial_t \vert \psi \vert^2 - \nabla \left[ \frac{\hbar^2}{2m \ii} \left( \psi^\ast \nabla \psi - \psi \nabla \psi^\ast \right) \right] = 0. \label{eq:2014-06-25-3}
\end{align*}
%
Gleichung~\eqref{eq:2014-06-25-3} entspricht der \acct{Kontinuitätsgleichung}, wobei
%
\begin{align*}
  \bm{v} &= \frac{\hbar}{2 m \ii} \frac{\left(  \psi^\ast \nabla \psi - \psi \nabla \psi^\ast \right)}{\vert \psi \vert^2}
\end{align*}
%
als Geschwindigkeitsfeld $\bm{v}$ interpretiert werden kann. Mit  $n = \vert \psi \vert^2$ folgt
%
\begin{align}
  \boxed{ \partial_t n - \nabla \left(n \bm{v}\right) = 0 }
\end{align}
%
die Teilchenzahlerhaltung. Betrachten wir den Ansatz für $\psi$
\begin{enumerate}
  \item $\psi = f \ee^{\ii \phi}$ und $|f|^2 = n$, $\bm{v} = \frac{\hbar}{m} \nabla \phi$. Hierbei ist $|f|^2$ die lokale Dichte und $\nabla \phi$ das lokale Geschwindigkeitsfeld. Es gilt
  %
  \begin{align*}
    \nabla \times \bm{v} = \frac{\hbar}{m} \nabla \times (\nabla \phi) \equiv 0.
  \end{align*}
  %
  D.h. das Geschwindigkeitsvektorfeld ist wirbelfrei. Ein Wirbel in einem Bose-Einsteinkondensat kann nur um einen Punkt verschwindender Dichte entstehen!
  \item Mit Ansatz  $\psi = f \ee^{\ii \phi}$ in die Gross-Piatevskii Gleichung~\eqref{eq:2014-06-25-1} und nach Real- un Imaginärteil sortieren. Der Imaginärteil führt auf die Kontinuitätsgleichung, der Realteil dagegen auf die \acct{Eulergleichung einer Quantenflüssigkeit}
  %
  \begin{align}
    \boxed{ \partial_t \bm{v} = - \frac{1}{mn} \nabla p - \nabla \left(\frac{v^2}{2}\right) + \frac{1}{m} \nabla \left(\frac{\hbar^2}{2 m \sqrt{n}} \nabla^2 \sqrt{n}\right)- \frac{1}{m} \nabla V}
  \end{align}
  %
  Dies wir auch als die \acct*{Bewegungsgleichung einer Quantenflüssigkeit} bezeichnet, wobei der Term \[ Q =  \frac{1}{m} \nabla \left(\frac{\hbar^2}{2 m \sqrt{n}} \nabla^2 \sqrt{n}\right) \] als \acct{Quantendruck} $Q$ angesehen wird. Die perfekte Flüssigkeit hat links den Term 
  %
  \begin{align*}
    \partial_t \bm{v} - \bm{v} \times (\nabla \times \bm{v}) = \ldots
  \end{align*}
  %
  und rechts verschwindet der Term 
  %
  \begin{align*}
    \frac{1}{m} \nabla \left(\frac{\hbar^2}{2 m \sqrt{n}} \nabla^2 \sqrt{n}\right)= 0.
  \end{align*}
  %
  Es gilt zudem \[ p \coloneq n^2 \frac{g}{2}\] für den Druck.
\end{enumerate}

\begin{notice}
  Unterschied eines Bose-Einsteinkondensat zur klassischen Flüssigkeit
  \begin{itemize}
    \item Superfluid ist wirbelfrei $\implies$ quantisierte Wirbel
    \item Quantendruck $\left(Q = \frac{1}{m} \nabla \left(\ldots\right)\right)$ $\implies$ Tunneleffekt 
  \end{itemize}
  Frage: Wie sieht die Dispersionsrelation einer Quantenflüssigkeit aus?
\end{notice}

