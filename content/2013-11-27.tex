% Vorlesung am 27.11.2013
% TeX: Michi

\renewcommand{\printfile}{2013-11-27}

\begin{theorem}[Definition]
  Zunächst definieren wir den \acct{Paritätsoperator} $\hat{P}$, welcher eine Spiegelung des Zustandes an der Null verursacht. Folgende Transformationen werden durch ihn veranlasst
  %
  \begin{align*}
    \bm{r} &\to -\bm{r} \\
    \theta &\to \theta - \pi  \\
    \phi &\to \phi  + \pi
  \end{align*}
  %
  $\theta$ wird reflektiert und $\phi$ rotiert (vgl. Abbildung~\ref{fig: 2013-11-27-0}).
\end{theorem}

\begin{figure}[htpb]
  \centering
  \tdplotsetmaincoords{75}{140}
  \begin{tikzpicture}[gfx,tdplot_main_coords]
    \coordinate (O) at (0,0,0);
    \tdplotsetcoord{P1}{3}{50}{90};
    \tdplotsetcoord{P2}{3}{180-50}{90};
    \tdplotsetcoord{P3}{3}{180-50}{180+90};
    \tdplotsetcoord{P2p}{1.3/sin(50)}{180-50}{90};
    \tdplotsetcoord{P3p}{1.3/sin(50)}{180-50}{180+90};
    \draw[->] (O) -- (0,0,3) node[left] {$z$};
    \draw[->] (O) -- (P1);
    \draw[->] (O) -- (P2);
    \draw[->] (O) -- (P3);
    \tdplotsetthetaplanecoords{90}
    \tdplotdrawarc[->,MidnightBlue,tdplot_rotated_coords]{(0,0,0)}{1.5}{0}{50}{anchor=south west}{$\theta$}
    \tdplotdrawarc[->,DarkOrange3,tdplot_rotated_coords]{(0,0,0)}{1}{0}{180-50}{anchor=south west}{$\pi - \theta$}
    \tdplotdrawarc[->,DarkRed]{(0,0,0)}{1.3}{-90}{90}{anchor=north east}{$\pi + \phi$}
    \draw[DarkRed,dotted] (P2p) -- (P2pxy);
    \draw[DarkRed,dotted] (P3p) -- (P3pxy);
  \end{tikzpicture}
  \caption{Spiegelung eines Zustandes am Ursprung  durch den Paritätsoperators $\hat{P}$}
  \label{fig: 2013-11-27-0}
\end{figure}

Es zeigt sich, dass die elektromagnetische Wechselwirkung invariant unter $\hat{P}$ ist, d.h.
\[
  \left[\hat{P},\hat{H}\right] = 0.
\]
Die Eigenwerte des Paritätsoperators ergeben sich zu $\pm 1$, da 
\[
  \hat{P}^2 = \mathds{1}
\] 
d.h. $\hat{P}$ entspricht einer Involution.

Die Kugelflächenfunktionen sind Eigenfunktionen zu $\hat{P}$, denn
%
\begin{align*}
  \hat{P} Y_{\ell m}(\theta,\phi) &= (-1)^\ell Y_{\ell m}. 
\end{align*}
%
Im folgenden wollen wir die Wirkung des Paritätsoperators auf den Winkelanteil des Dipol-Matrixelements untersuchen, also
%
\begin{align*}
  \hat{P} \int Y_{f}^\ast Y_{1 m} Y_{i} \diff \Omega &= (-1)^{\ell_i + \ell_f +1} \int Y_{f}^\ast Y_{1 m} Y_{i} \diff \Omega,
\end{align*}
%
d.h $\Delta \ell$ muss ungerade sein (insbesondere $\Delta \ell \neq 0$).

\begin{notice}
  \begin{enumerate}
    \item Die Parität muss beim Dipolübergang verschieden sein, d.h. Anfangs- und Endzustand haben unterschiedliche Parität, da das Dipol-Matrixelement antisymmetrisch ist.
    \item Das Dipolmoment kann nicht größer sein als der Überlapp beider Wellenfunktionen. Für Experimente bedeutet dies, dass ein stärkerer Laser und damit verbunden mehr Geld benötigt wird.
  \end{enumerate}
\end{notice}

\section{Das Heliumatom}

Das \acct{Heliumatom} entspricht einem schwereren Problem als dem des Wasserstoffatom, da es ähnlich dem Drei-Körper-Problem in der klassischen Mechanik nicht exakt lösbar ist. Abbildung~\ref{fig: 2013-11-27-1} veranschaulicht das Problem. 
 
\begin{figure}[htpb]
  \centering
  \begin{tikzpicture}[gfx]
    \node[circle,draw,DarkOrange3] (K) at (0,0) {$++$};
    \node[circle,draw,MidnightBlue,label={150:$\color{MidnightBlue}e^-$}] (e1) at (-1,2) {$-$};
    \node[circle,draw,MidnightBlue,label={-40:$\color{MidnightBlue}e^-$}] (e2) at (2,1) {$-$};
    \draw[MidnightBlue] (K) -- node[below left] {$r_1$} (e1);
    \draw[MidnightBlue] (K) -- node[below right] {$r_2$} (e2);
  \end{tikzpicture}
  \caption{Prinzipielle Struktur des Heliumatoms}
  \label{fig: 2013-11-27-1}
\end{figure}

Der Hamiltonoperator ist durch
%
\begin{align*}
  \hat{H} &= \hat{H}_{1} + \hat{H}_2 + \hat{H}_{12}
\end{align*}
%
gegeben, wobei $\hat{H}_{12}$ der Coulombwechselwirkung der beiden Elektronen entspricht. Im folgenden soll eine Lösung mittels geeigneter Näherungen gefunden werden. Folgende grobe Näherungen werden hier beachtet:
\begin{enumerate}
  \item Vernachlässigung aller relativistischen Effekte:
    \begin{itemize}
      \item $\bm{\ell}_1 \cdot \bm{s}_1$, $\bm{\ell}_2 \cdot \bm{s}_2$ -- Spin-Bahn Wechselwirkung
      \item $\bm{\ell}_1 \cdot \bm{s}_2$, $\bm{\ell}_2 \cdot \bm{s}_1$ -- Spin-other-orbit Wechselwirkung
      \item $\bm{s}_1 \cdot \bm{s}_2$ -- Spin-Spin Wechselwirkung
      \item$\bm{\ell}_1 \cdot \bm{\ell}_2$ -- Bahn-Bahn Wechselwirkung 
    \end{itemize}
  \item Vernachlässigung von ${H}_{12} = \frac{e^2}{4 \pi \varepsilon_0 r_{12}}$ (Coulomb-Term)
\end{enumerate}

Ein geeigneter Lösungsansatz ist
%
\begin{align*}
  \psi_0 &= u_{n \ell m}(r_1) \cdot u_{n' \ell' m'}(r_2) \\
  &= u_a(1)\cdot u_b(2) \\
  &= u(ab)
\end{align*}
%
Hierbei sind $u_{n \ell m}$ und $u_{n' \ell' m'}$ die Wasserstoffwellenfunktionen. Für die Energien ergibt sich
\[
  E = E_{n} + E_{n'}.
\]
Wenn die Wellenfunktionen überlappen, kann Elektron 1 und Elektron 2 nicht unterschieden werden, d.h. es existiert eine entartete gleichberechtigte Lösung
%
\begin{align*}
  \tilde{\psi}_0 &= u_a(2) \cdot u_b(1) \\
  &= u_{ba},
\end{align*}
%
also der Wechsel der Indizes von $u_{ab}$. Die allgemeine Lösung setzt sich aus beiden Lösungen zusammen 
%
\begin{align*}
  u &= \alpha u_{ab} + \beta u_{ba}.
\end{align*}
%
$\hat{H}_{12}$ hebt die Entartung auf, da $H_{12}$ invariant unter $\hat{P}$ ist,
\[
  \hat{P} H_{12} = + H_{12},
\]
d.h. es werden Zustände gleicher Parität gekoppelt. Der Hamiltonoperator $\hat{H}_{12}$ ergibt sich somit zu
%
\begin{align}
  \hat{H}_{12} &= \begin{pmatrix}
    \Braket{u_{ab}|H_{12}|u_{ab}} & \Braket{u_{ab}|H_{12}|u_{ba}} \\
    \Braket{u_{ba}|H_{12}|u_{ab}} & \Braket{u_{ba}|H_{12}|u_{ba}}
  \end{pmatrix}
\end{align}
%
Die Diagonalterme, auch direktes Integral genannt ergeben sich zu 
%
\begin{align*}
  \Braket{u_{ab}|H_{12}|u_{ab}} &= \int\frac{(-e) \left|u_a(1)\right|^2 (-e) \left|u_b(2)\right|^2}{4 \pi \varepsilon_0 r_{12}} \diff V_1 \diff V_2 \\
  &= \int\frac{\varrho_a(1) \cdot \varrho_b(2)}{4 \pi \varepsilon_0 r_{12}} \diff V_1 \diff V_2 \\
  & \int\frac{\varrho_b(1) \cdot \varrho_a(2)}{4 \pi \varepsilon_0 r_{12}} \diff V_1 \diff V_2 \\
  &= \Braket{u_{ba}|H_{12}|u_{ba}}
\end{align*}
%
Die \acct*{elektrostatische Abschirmung} \index{Abschirmung ! elektrostatisch}ist hierbei wie folgt definiert
%
\begin{align*}
  J \coloneq \int\frac{\varrho_a(1) \cdot \varrho_b(2)}{4 \pi \varepsilon_0 r_{12}} \diff V_1 \diff V_2
\end{align*}
%

Die Außerdiagonalen Terme, auch Austauschintegral $K$ genannt entsprechen einem \frqq quantenmechanischen Interferenzterm\flqq\ aufgrund der Ununterscheidbarkeit der beiden Elektronen. Er misst den Überlapp der Wellenfunktionen. 
%
\begin{align*}
  \det\begin{pmatrix}
    J-\Delta E & K \\
    K & j+ \Delta E
  \end{pmatrix}
  &= 0 
\end{align*}
%
Dies führt auf die Eigenwerte
%
\begin{align*}
  \Delta E &= J \pm K
\end{align*}
%
die die Aufspaltung in Abbildung~\ref{fig: 2013-11-27-2} erklären.

\begin{figure}[htpb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (0,-0.5) -- (0,2) node[left] {$E$};
    \draw (0.1,0) -- (-0.1,0) node[left] {$E_n + E_{n'}$};
    \begin{scope}[every path/.append style={DarkOrange3}]
      \draw (0.5,0) -- (2,0);
      \draw[dashed] (2,0) -- (3,1);
      \draw (3,1) -- (4.5,1);
      \draw[dashed] (4.5,1) -- (5,0.5) (4.5,1) -- (5,1.5);
      \draw (5,0.5) -- (6,0.5) node[right] {$U_A = \dfrac{1}{\sqrt{2}} (u_{ab} - u_{ba})$};
      \draw (5,1.5) -- (6,1.5) node[right] {$U_s = \dfrac{1}{\sqrt{2}} (u_{ab} + u_{ba})$};
      \draw[<->] (3.75,0) -- node[right] {$J$} (3.75,1);
      \draw[<->] (5.5,0.5) -- node[right] {$2K$} (5.5,1.5);
    \end{scope}
  \end{tikzpicture}
  \caption{Aufspaltung der Energieniveaus im Heliumatom.}
  \label{fig: 2013-11-27-2}
\end{figure}

Um der Frage nach zu gehen ob Übergänge zwischen $u_s$ und $u_A$ erlaubt sind, betrachten wir zunächst den \acct{Austauschoperator} $\hat{P}_{12}$, dieser hat die Eigenschaft
%
\begin{align*}
  \hat{P}_{12} u_A &= -u_A \\
  \hat{P}_{12} u_s &= u_s.
\end{align*}
%
Betrachten wir die Wirkung des Austauschoperators auf den Dipoloperator
%
\begin{align*}
  \hat{P}_{12} \hat{\bm{d}}_{12} &= \hat{P}_{12} \left(e \bm{r}_1 +e \bm{r}_2 \right) \\
  &= \left(e \bm{r}_1 +e \bm{r}_2 \right) \\
  &= + \hat{\bm{d}}_{12}.
\end{align*}
%
Wirkt der Austauschoperator auf $d_{12}$ so gilt
%
\begin{equation}
  \begin{split}
    \hat{P}_{12} \int u_s^\ast d_{12} u_A \diff \Omega &= - \int u_s^\ast d_{12} u_A \diff \Omega \\
    &= 0
  \end{split}
  \label{eq: 2013-11-27-1}
\end{equation}
%
Der Austausch der Bezeichnungen der beiden Elektronen in Abbildung~\ref{fig: 2013-11-27-2} darf den Wert des Integrals nicht ändern, weshalb das in Gleichung~\eqref{eq: 2013-11-27-1} berechnete Matrixelement $0$ ist. Die Symmetrieeigenschaft bezüglich $\hat{P}_{12}$ bleibt erhalten. Mit andern Worten, $\hat{P}_{12}$ ist eine Konstante der Bewegung. 

\begin{notice}
  Die ganze Rechnung ist nur erlaubt falls 
  \[
    \Delta E \ll E_n,E_n'
  \]
  ist (erste Ordnung Störungstheorie).
\end{notice}

Im Grundzustand $E(1s^2)$ verschwindet $u_A$. Für die Energie gilt
%
\begin{align*}
  E\left(1s^2\right) &= 2 E(1s) + \Delta E.
\end{align*}
%
Hierbei entspricht $E(1s)$ der Grundzustandsenergie im Wasserstoffatom, also \SI{-54.4}{\eV}, $\Delta E$ berechnet sich aus
\[
  \Braket{\frac{e^2}{4\pi \varepsilon_0 r_{12}}} = \frac{1}{4 \pi \varepsilon_0} \frac{5}{4} Z \frac{e^2}{2 a_0} = \SI{34}{\eV}.
\]
Abbildung~\ref{fig: 2013-11-27-3} stellt den Grundzustand mit den von uns verwendeten Ansatz dar, sowie einen durch numerische Methoden bestimmten Wert. Die Abbildung zeigt, dass unser Lösungsansatz nicht die Exaktheit des numerischen Wertes entspricht und dementsprechend verbessert werden muss.  

\begin{figure}[htpb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw (1,0) -- (11,0) node[right] {He$^{++}$};
    \draw[<->] (2,0) -- node[left] {$\SI{-108.8}{\eV}$} (2,-8);
    \draw (1,-8) -- node[below] {$1s^2$ ohne H$_2$} (3,-8);
    \draw[<->] (4,0) -- node[right] {$\SI{-54.4}{\eV}$} (4,-4);
    \draw (3,-4) -- (11,-4) node[right] {He$^+$ ($1s$)};
    \draw[<->] (4,-4) -- node[right] {$\SI{-54.4}{\eV} + \SI{34}{\eV} = \SI{-20.4}{\eV}$} (4,-6);
    \draw (3,-6) -- node[below] {$1s^2$} (5,-6);
    \draw[<->] (9,-4) -- node[right] {$\SI{-24.58}{\eV}$} (9,-6.5);
    \draw (8,-6.5) -- node[below] {He ($1s^2$) numerische Lösung} (10,-6.5);
  \end{tikzpicture}
  \caption{Grundzustandsenegie des Heliumatoms unserer Rechnung und im Vergleich dazu der numerisch ermittelte Wert}
  \label{fig: 2013-11-27-3}
\end{figure}

\subsection{Angeregte Zustande}
Im Folgenden wollen wir nun auch angeregte Zustanände betrachten, wie z.B. für ein angeregte Elektron, sodass wir folgende Konfiguration erhalten
\[
  (1s)(n \ell).
\] 
Die erste Klammer entspricht dem inneren Elektron die zweite dem äußeren Elektron. Die Energie bezüglich $\mathrm{He}^{+}(1s)$ ist dann
\[
  E = E_n + J \pm K. 
\]
$J$ ist dafür verantwortlich, dass die $\ell$-Entartung (vgl Wasserstoffatom) aufgehoben wird, da die Exzentrizität des Zustands von $\ell$ abhängt.

Für hohe $n$ ist die Abschirmung somit immer besser, d.h. Energien nähern sich dem des Wasserstoffatoms an.
