% Vorlesung am 29.1.2014
% TeX: Michi

\renewcommand{\printfile}{2014-01-29}

Im Gleichgewichtszustand gilt für die Blochgleichungen
%
\begin{align*}
  \begin{pmatrix} \dot{u} \\ \dot{v} \\ \dot{w}  \end{pmatrix} &= 0.
\end{align*}
%
Dies führt zu folgendem Blochvektor
%
\begin{align*}
  \bm{R} &= \begin{pmatrix} {u} \\ {v} \\ {w}  \end{pmatrix} \\
  &= \frac{1}{\delta^2 + \frac{\omega_R^2}{2} + \frac{\Gamma^2}{4} }  \begin{pmatrix} \omega_R \delta \\ \omega_R \frac{\Gamma}{2} \\ \delta^2+ \frac{\Gamma^2}{4}  \end{pmatrix}.
\end{align*}
%
Die Darstellung lässt sich durch Einführen einer dimensionslosen Zahl, dem \acct{Sättigungsparameter} 
%
\begin{align}
  \boxed{ S\coloneq \frac{\omega_R^2/2}{\delta^2+ \Gamma^2/4} }
\end{align}
%
verschönern. Für das Matrixelement $\varrho_{22}$ der Dichtematrix bedeutet dies
%
\begin{align*}
  \varrho_{22} &= P_e \\
  &= \frac{1- w}{2} \\
  &= \frac{\omega_R^2/4}{\delta^2 + \omega_R^2/2+ \Gamma^2/4} \\
  &= \frac{1}{2}\frac{S}{S+1}.
\end{align*}
%
Filt $S\ll1$, so ist man im linearen Bereich, wie vom Lorentzmodell bestätigt. Erst für größere $S$ tritt Abweichung zum Lorentzmodell auf. Abbildung~\ref{fig:2014-01-29-1} zeigt dieses Verhalten. Für große $S$ konvergiert $\varrho_{22}$ gegen die Wahrscheinlichkeit $1/2$.

\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}[gfx,y=4cm]
    \draw[->] (-0.3,0) -- (4,0) node[below] {$s$};
    \draw[->] (0,-0.3/4) -- (0,0.65) node[left] {$P_e$};
    \draw (0.1,0.25) -- (-0.1,0.25) node[left] {$\frac{1}{4}$};
    \draw (0.1,0.5) -- (-0.1,0.5) node[left] {$\frac{1}{2}$};
    \draw (1,0.1/4) -- (1,-0.1/4) node[below] {$1$};
    \draw plot[domain=0:4] (\x,{0.5*\x/(\x+1)});
    \draw[dotted,DarkOrange3] plot[domain=0:2.7] (\x,0.25*\x) node[right] {Lorentz-Modell (klassisch)};
    \draw[dashed] (0,0.25) -- (1,0.25) -- (1,0);
  \end{tikzpicture}
  \caption{Wahrscheinlichkeit in $\ket{e}$ zu sein in Abhängigkeit des Sättigungsparameter versus dem Lorentzmodell}
  \label{fig:2014-01-29-1}
\end{figure} 

\section{Lichtkräfte und Laserkühlung}

\begin{theorem}
  Das \acct{Ehrenfest-Theorem} lautet
    %
    \begin{align*}
      \frac{\diff}{\diff t} \braket{A} = - \ii \hbar \Braket{\left[ H,A \right]} + \Braket{\partial_t A }.
    \end{align*}
    %
  $A$ ist hierbei ein quantenmechanischer Operator (physikalische Größe) der im Allgemeinen zeitabhängig ist und $H$ der Hamiltonoperator des Systems.
\end{theorem}

\begin{notice}
   Das Ehrenfestsche Theorem besagt, dass sich quantenmechanische Observablen im Mittel klassisch Verhalten. Außerdem gilt:
   \begin{itemize}
     \item Verschwindet der Kommutator $[A,H] = 0$ und ist zudem $\partial_t A = 0$ bleibt der Erwartungswert erhalten. Es folgt dann:
     %
     \begin{align*}
       \frac{\diff}{\diff t} \mathrm{prob}[A \hateq a_n|\psi(t)] = 0.
     \end{align*}
     %
     \item Ein klassisches Analogon stellt hierbei die Poisson Klammer dar
     %
     \begin{align*}
       \frac{\diff}{\diff t} f(q_i,p_i,t) &= \{f,H\} + \partial_t f.
      \end{align*}
     %
   \end{itemize}
  Das Ehrenfest-Theroem erlaubt eine klassische Näherung, indem wir den Eigenwert der Kraft $F(x)$ in eiener Taylorreihe entwickeln um den Erwartungswert $\braket{x}$, d.h. 
  %
  \begin{align*}
    \braket{F(x)} &= \Braket{ F(\braket{x}) + F'(\braket{x}) (x-\braket{x}) + \frac{1}{2} F''(\braket{x}) (x-\braket{x})^2 + \mathcal{O}(x^3) } \\
    &=  F(\braket{x}) + F'(\braket{x})\underbrace{ \Braket{(x-\braket{x})}}_{=0} + \frac{1}{2} F''(\braket{x}) \underbrace{\Braket{(x-\braket{x})^2}}_{=(\Delta x)^2}+ \Braket{ \mathcal{O}(x^3) } \\
    &= F(\braket{x}) + \frac{1}{2} F''(\braket{x}) (\Delta x)^2 + \Braket{ \mathcal{O}(x^3) }
  \end{align*}
  %
  Wird nur der erste Summand betrachtet (nur erlaubt, falls die Kraft eine lineare Funktion ist), also \[ \Braket{F(x)} \approx F(\braket{x}), \] so erhält man eine Differentialgleichung der Form \[ m \frac{\diff ^2}{\diff t^2} \braket{x} = F(\braket{x}). \] Die Erwartungswerte bewegen sich also auf klassische Bahnen, d.h. das Ehrenfest-Theorem liefert uns direkt die Newtonsche Bewegungsgleichung, wenn wir nach dem Korrespondenzprinzip die Operatoren durch klassische Größen ersetzen. 
\end{notice}

\subsection{Dipolkraft und spontane Lichtkraft}

\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}[gfx]
    \node[draw,circle,MidnightBlue] (p) at (2,1) {$+$};
    \node[dot,DarkOrange3,label={[DarkOrange3]right:$e^-$}] (e) at (2,2) {};
    \draw[MidnightBlue,->] (0,0) node[dot,label={below:$O$}] {} -- node[below] {$\bm{R}$} (p);
    \draw[DarkOrange3,->] (p) -- node[left] {$\bm{r}$} (e);
  \end{tikzpicture}
  \caption{Zur Berechnung der gemittelten Kraft und der Näherung}
  \label{fig:2014-01-29-2}
\end{figure} 

Zunächst berechnen wir die gemittelte Kraft mit Hilfe der obigen Bemerkung
%
\begin{align*}
  m_{\text{Atom}} \Braket{\ddot{R}} &= \Braket{\nabla_\bm{R} \left(\bm{d} \cdot \bm{E}\right)}.
\end{align*}
%
Der Nabla-Operator wirkt hierbei nur auf $\bm{R}$, da $\lambda \gg \bm{d}$, bzw. $\bm{r}$ nur wenig Angström beträgt (Vgl. Abbildung~\ref{fig:2014-01-29-2}). Insbesondere gilt
%
\begin{align*}
  m_{\text{Atom}} \Braket{\ddot{R}} &= \sum\limits_{j = x,y,z} \braket{d_j} \cdot \nabla E_j.
\end{align*}
%
Im Folgenden berücksichtigen wir die Näherung, dass mit punktförmigen Teilchen gerechnet werden kann, also \[ \braket{\ddot{R}} = \ddot{R}, \] da die DeBroglie-Wellenlänge viel kleiner ist als die des Feldes (optisch), also \[ \lambda_{\mathrm{dB}} \ll \lambda_{\text{optisch}}. \]

Jetzt betrachten wir ein \frqq ruhendes Atom\flqq, d.h. wir bekommen die Gleichgewichtslösungen für $\braket{d_j}$. 
%
\begin{align*}
  \braket{\hat{\bm{d}}} &=  \tr\left[\varrho \cdot \hat{\bm{d}}\right] \\
  &=   \bm{d}_{12} \left(\varrho_{12} + \varrho_{21}\right) \\
  &= 2 \bm{d}_{12} \left(u_{\mathrm{st}} \cos \omega_Lt - v_{St} \sin \omega_Lt \right)
\end{align*}
%

\begin{notice}
  Nur die Außerdiagonalelemente geben ein Dipolmoment, d.h. liefern einen Beitrag.
\end{notice}

Die Bestimmung von $u_{\mathrm{st}}$, bzw. $v_{\mathrm{st}}$ soll anhand verschiedener Überlegungen erfolgen. So betrachten wir folgendes elektrisches Feld
%
\begin{align*}
  \bm{E} &= \bm{\varepsilon} E_0(\bm{R}) \cos\left(\omega_Lt+\phi(\bm{R})\right).
\end{align*}
%
Gradient auf eine der Komponenten von $\bm{E}$ ergibt
%
\begin{align*}
  \nabla E_j &= \varepsilon_j \cos\left(\omega_Lt+\phi\right) \cdot \nabla E_0 - \varepsilon_j E_0 \sin\left(\omega_L t+\phi\right) \cdot \nabla \phi.
\end{align*}
%
Die \acct*{Lichtkraft} ist somit:
%
\begin{align}
  \bm{F} &= \overline{\sum \braket{d_j} \nabla E_j}^t \\
  &= \bm{\varepsilon} \cdot \bm{d}_{12} \left[ u_{\mathrm{St}} \nabla E_0 + v_{\mathrm{St}} E_0 \nabla \phi \right]
\end{align}
%
Hierbei ist der Term $u_{\mathrm{St}} \nabla E_0$ die in Phase \acct{Dipolkraft} und $ v_{\mathrm{St}} E_0 \nabla \phi$ die spontane Lichtkraft (wird auch als Lichtdruck bezeichnet). Im zuge der Rechnung wurde beachtet, dass in  zeitlichen Mittel über eine optische Periode sowohl Sinus als auch Kosinus verschwinden. Die Klammer kann wie folgt umgeschrieben werden
%
\begin{align*}
  \bm{F}_{\text{Dipol}} &= - \frac{\hbar \omega_R u_{\mathrm{St}}}{\omega_R} (\nabla \omega_R) \\
  \bm{F}_{\text{spont}} &= \hbar \omega_R v_{\mathrm{St}} (\nabla \phi).
\end{align*}
%

\begin{example}
  \begin{itemize}
    \item Für eine laufende Welle mit $\bm{E} = \bm{\varepsilon} E_0 \cos(\omega_Lt - \bm{k}_L \cdot \bm{r})$ folgt:
    %
    \begin{align*}
      \phi(\bm{R}) &= - \bm{k}_L \cdot \bm{r} \\
      \implies \nabla \phi &= - \bm{k}_L
    \end{align*}
    %
    Die spontane Lichtkraft ist somit
    %
    \begin{align*}
      \bm{F}_{\text{spont}} &= \omega_R v_{\mathrm{St}} \hbar \bm{k}_L \\
      &= \frac{\Gamma}{2} \frac{S}{S+1} \hbar \bm{k}_L
    \end{align*}
    %
    Es zeigt sich, dass die spontane Lichtkraft bei $\frac{\Gamma}{2} \hbar \bm{k}_L$ sättigt.
    \item Eine stehende Welle (mithilfe zwei laufender Wellen)
    %
    \begin{align*}
      \bm{E} &= \bm{\varepsilon} E \cos(k_L z) \cos(\omega_L t)
    \end{align*}
    %
    Berechnung der Dipolkraft 
    %
    \begin{align*}
      \overline{\bm{F}}_{\text{Dipol}}^t &= - \hbar \delta \frac{S}{S+1} \frac{1}{\omega_R} \nabla \omega_R \\
      &= \delta \frac{S}{S+1} \hbar k_L \tan(k_L z)
    \end{align*}
    %
  \end{itemize}
\end{example}

\begin{notice}
  \begin{enumerate}
    \item Für $\delta = 0$ ist die zeitlich gemittelte Dipolkraft \[ \overline{\bm{F}}_{\text{Dipol}}^t = 0, \] aber große Fluktuationen.
    \item $\overline{\bm{F}}_{\text{Dipol}}^t$ ist nicht wie die zeitlich gemittelte spontane Lichtkraft $\overline{\bm{F}}_{\text{spont}}^t$ nach oben begrenzt. Für jedes $\omega_R$ gibt es ein $\delta$, dass die gemittelte Dipolkraft $\overline{\bm{F}}_{\text{Dipol}}^t$ maximiert. Typischer Wert ($\omega_R \gg \Gamma$) zeigt \[ \delta \propto \omega_R, \] und somit
    %
    \begin{align*}
      \overline{\bm{F}}_{\text{Dipol}}^t &= \hbar  k \omega_R.
    \end{align*}
    % 
    D.h. stimulierte Absorption-, bzw. Emissionszyklen.
  \end{enumerate}
\end{notice}
