% Vorlesung am 12.04.2013
% TeX: Jan

\renewcommand{\printfile}{2013-12-11}

\section{Rydbergatome}

\subsection{Grundlegende Eigenschaften von Rydbergatomen}
%
Ein \acct{Rydberg-Atom} liegt dann vor, wenn sich das äußerste Elektron in einem Zustand mit Hauptquantenzahl $n$ befindet, die weit über den bei Atomen im Grundzustand vorkommenden Maximalwert von $n = 6$ liegt. Gemäß dem \acct*{Korrespondenzprinzip} geht dabei die quantenmechanische Beschreibung in die klassische Beschreibung über, wodurch gewisse Effekte im Rahmen der klassischen Mechanik beschrieben und verstanden werden können.

In Abbildung~\ref{fig: hydrogen43s} ist die Aufenthaltswahrscheinlichkeitsdichte, sowie die Aufenthaltswahrscheinlichkeit des $43s$-Zustands (nur Radialteil) beim Wasserstoffatom über dem Atomradius aufgetragen.
%

\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}[gfx,/pgf/number format/1000 sep={\,}]
    \begin{axis}[
        gfx,
        width=0.45\textwidth,
        height=4cm,
        no markers,
        scaled ticks=false,
        enlargelimits=false,
        legend style={draw=none,fill=none},
        xlabel={$r$ in Einheiten von $a_0$},
        xmin=0,
      ]
      \addplot+[MidnightBlue] table {content/hydrogen43s_R(43,0,r).dat};
      \addlegendentry{$R_{43,0}(r)^2$};
    \end{axis}
  \end{tikzpicture}
  \quad
  \begin{tikzpicture}[gfx,/pgf/number format/1000 sep={\,}]
    \begin{axis}[
        gfx,
        width=0.45\textwidth,
        height=4cm,
        no markers,
        scaled ticks=false,
        enlargelimits=false,
        legend style={draw=none,fill=none},
        legend pos={north west},
        xlabel={$r$ in Einheiten von $a_0$},
      ]
      \addplot+[Purple] table {content/hydrogen43s_r*R(43,0,r).dat};
      \addlegendentry{$r^2 R_{43,0}(r)^2$};
    \end{axis}
  \end{tikzpicture}
  \caption{Aufenthaltswahrscheinlichkeitsdichte (blau) und Aufenthaltswahrscheinlichkeit (lila) des $43s$-Zustands beim Wasserstoffatoms. Signifikant ist hierbei vor allem die hohe Aufenthaltswahrscheinlichkeit am Umkehrpunkt.}
  \label{fig: hydrogen43s}
\end{figure}
%
Anhand dieser Abbildung wird der Übergang zur klassischen Mechanik besonders deutlich. An den Umkehrpunkten, i.e in der nähe des Atomradius ist die Aufenthaltswahrscheinlichkeit am größten, was in völliger Analogie zum Klassischen ist (man denke dabei z.B. an einen harmonischen Oszillator). Des weiten stellen wir fest, dass die Aufenthaltswahrscheinlichkeit nahe am Kern verschwindend gering ist, wohingegen diese beim Wasserstoff-Grundzustand dort ihr Maximum besitzt (am Kernort selbst ist auch beim Grundzustand die Aufenthaltswahrscheinlichkeit Null). Dies ist ein typisches Merkmal für Rydbergatome, wobei diese Aufenthaltswahrscheinlichkeit noch kleiner wird, wenn der Drehimpuls maximiert wird. Weitere typische Eigenschaften von Rydbergatomen sind:
%
\begin{itemize}
  \item Die \acct*{Bindungsenergie} skaliert mit $\frac{1}{n^2}$. Dies ist auf die Tatsache zurückzuführen, dass aufgrund der hohen Anregung Feinstruktur- und Hyperfeinstrukturwechselwirkung vernachlässigt werden können und damit die Bindungsenergie durch $E_n = -\frac{R_y}{n^2}$ mit der Rydbergkonstanten $R_y$ gegeben ist.
  \item Die \acct*{Aufspaltung der Energieniveaus} skaliert mit $\frac{1}{n^3}$
  \item Der \acct*{Atomradius} nimmt mit $n^2$ zu.
  \item Die \acct*{Lebensdauer} (spontaner Zerfall) von Rydbergzuständen nimmt mit $n^3$ zu.\\ Durch die Schwarzkörperstrahlung kommt es allerdings zu stimulierten Emissionsprozessen, wodurch die Lebensdauer verkürzt wird (dazu später mehr)
  \item Die \acct*{Dipolmomente} skalieren mit $\approx n^2$
  \item Die \acct*{Polarisierbarkeit} skaliert mit $n^7$
\end{itemize}
%
\subsubsection{Zur Lebensdauer von Rydbergatomen (am Beispiel von Wasserstoff und Rubidium)}
%
Anhand der obigen Auflistung sehen wir, dass Rydbergatome $\approx 1000$ mal länger Leben als normale angeregte Zustände. Die Linienbreite eines Übergangs zwischen zwei Niveaus ist dabei durch den Einstein-A-Koeffizienten gegeben:
%
\begin{align*}
  \Gamma &= A_{n\ell\rightarrow n'\ell'} = \frac{\omega^3}{3\pi\varepsilon_0\hbar c^3}\left|\Braket{n,\ell,m_{\ell}|\hat{\bm{d}}|n',\ell',m_{\ell'}}\right|^2
\end{align*}
%
Daraus ergibt sich schließlich die Lebensdauer eines angeregten Zustands gemäß
%
\begin{align*}
  \tau^{-1}_{\text{spont}} &= \Gamma_{\text{tot}} = \sum_{n',\ell'}A_{n\ell\rightarrow n'\ell'}
\end{align*}
%
Neben diesen Zerfällen durch spontante Emission von Photonen kommt es aber auch durch die Schwarzkörper-strahlung zu stimulierten Emissionsprozessen, welche die Lebensdauer der angeregten Zustände verkürzen. Die Rate für stimulierte Emission ist dabei durch den effektiven Einsteinkoeffizienten $\tilde{A} = \bar{n}A$ gegeben. Hierbei ist $\bar{n} = \frac{1}{e^{\hbar\omega/k_B T} - 1}$ die mittlere Photonenzahl der Schwarzkörperstrahlung. Die durch Schwarzkörperstrahlung induzierten Lebensdauern sind dabei gegeben durch
%
\begin{align*}
  \tau^{-1}_{bb} &= \sum_{n'\ell'}\tilde{A}_{n\ell\rightarrow n'\ell'}
\end{align*}
%
Die resultierende Lebensdauer ergibt sich dann aus Superposition beider einzelnen Lebensdauern also $\tau_{\text{spont} + bb}$. Dabei berechnet man für den $43s$-Zustand des Wasserstoffatoms:
%
\begin{align*}
  \tau_{\text{spont} + bb} &= \SI{46}{\micro\s}
\end{align*}
%
Bei Raumtemperaturen dominiert hierbei jedoch der Zerfall durch die Schwarzkörperstrahlung. Demnach bietet es sich an die Temperatur so weit wie möglich zu senken, wenn man mit Rydbergatomen arbeitet.

\subsection{Rydbergatome im elektrischen Feld}
%
Wegen ihrer vergleichsweise großen räumlichen Ausdehnung und der großen Anzahl an eng benachbarten, bzw. fast entarteten Energieniveaus reagieren Rydbergatome besonders sensitiv auf elektrische Felder. Dabei weisen jedoch Rydbergzustände mit unterschiedlichen $\ell$-Quantenzahlen verschiedene Stark-Effekte auf. Nicht entartete Zustände weisen dabei einen quadratischen Stark-Effekt auf und entartete Zustände den linearen Stark-Effekt. Dies ist in den Abbildung~\ref{fig: starkhydrogen} und Abbildung~\ref{fig: starkrubidium} für Wasserstoff und Rubidium dargestellt.
%
\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[gfx,
      enlargelimits=false,
      axis on top,
      xlabel={elektrisches Feld (\si{\V\per\m})},
      ylabel={Energie (\si{\eV})},
      ytick = {-0.22,-0.20,...,-0.04},
      yticklabel style={/pgf/number format/.cd,fixed},
    ]
      \addplot graphics[xmin=0,xmax=3e6,ymin=-0.24,ymax=-0.05] {content/StarkEffectHydrogen};
      \begin{scope}[every node/.style={font=\tiny,right}]
        \node at (axis cs:0,-0.075) {$n=14$};
        \node at (axis cs:0,-0.087) {$n=13$};
        \node at (axis cs:0,-0.102) {$n=12$};
        \node at (axis cs:0,-0.120) {$n=11$};
        \node at (axis cs:0,-0.142) {$n=10$};
        \node at (axis cs:0,-0.175) {$n= 9$};
        \node at (axis cs:0,-0.220) {$n= 8$};
      \end{scope}
    \end{axis}
  \end{tikzpicture}
  \caption{Stark-Effekt im Wasserstoff}
  \label{fig: starkhydrogen}
\end{figure}
%
\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[gfx,
      enlargelimits=false,
      axis on top,
      xlabel={elektrisches Feld (\si{\V\per\centi\m})},
      ylabel={Laserfrequenz relativ zum $5P_{3/2}$-Niveau (\si{\tera\Hz})},
      ytick = {623.550,623.600,...,623.800},
      yticklabel style={/pgf/number format/.cd,fixed,fixed zerofill,precision=3},
    ]
      \addplot graphics[xmin=0,xmax=30,ymin=623.520,ymax=623.750] {content/StarkEffectRubidium};
      \begin{scope}[every node/.style={font=\tiny,right,inner ysep=1pt}]
        \node at (axis cs:3.1,623.740) {$n\mathord{=}40$,$I\mathord{>}F$};
        \node at (axis cs:3.3,623.635) {$n\mathord{=}39$,$I\mathord{>}F$};
        %
        \node at (axis cs:0,623.731) {$40F$};
        \node at (axis cs:0,623.720) {$43S$};
        \node at (axis cs:0,623.695) {$41D$};
        \node at (axis cs:0,623.665) {$42P$};
        \node at (axis cs:0,623.625) {$39F$};
        \node at (axis cs:0,623.610) {$42S$};
        \node at (axis cs:0,623.590) {$40D$};
        \node at (axis cs:0,623.555) {$41P$};
      \end{scope}
    \end{axis}
  \end{tikzpicture}
  \caption{Stark-Effekt im Rubidium}
  \label{fig: starkrubidium}
\end{figure}
%
\subsection{Sonstiges}
%
\subsubsection{Detektion von Rydbergatomen}
%
Da es sich bei Rydbergatomen um stark angeregte Elektronenzustände handelt, deren Energie unweit von der Kontinuumsschwelle entfernt ist, können diese sehr leicht ionisiert werden. So führt bereits etwa eine Störung mit der Umgebung zur Ionisierung. Dadurch kommt ein Elektronenfluss zustande, der dazu genutzt werden kann, um Rydbergatome zu detektieren.
%
\subsubsection{Verwendung von Rydbergatomen}
%
Dadurch das Rydbergzustände (vor allem bei niedrigen Temperaturen) besonders langlebig sind, stellen Rydbergatome einen bedeutenden Schritt in Richtung zur Realisierung eines Quantencomputers dar.
%
